# Example Toy Data

* `reads/` stores example query reads.
* `reference/` stores minimal reference data:
  * `fasta/` stores reference sequences
  * `nodes.dmp` stores taxonomy in a minimal format (however the full NCBI format is expected)
  * `names.dmp` stores taxonomic names in a minimal format (however the full NCBI format is expected)
  * `example.accession2taxid` stores mapping between sequence identifiers and taxonomic tree nodes
