#!/bin/bash

usage() {
    echo "usage: $0 [-h] [-S DIR]"
    echo "options:"
    echo "  -h        display this help"
    echo "  -S <DIR>  path to SBT installation"
}

DNASBT_BIN_PREFIX=""

DATA="../../data"
DB="example-db"

while getopts ":hS:" arg; do
    case $arg in
      h)
        usage
        exit -1
        ;;
      S)
        DNASBT_BIN_PREFIX=$(readlink -f "$OPTARG")/bin/
        ;;
      ?)
        usage
        exit -1
        ;;
    esac
done

if [ -z "$DNASBT_BIN_PREFIX" ]; then
  usage
  echo
  echo "path to SBT installation missing"
  exit -1
fi

# first we index reference data to create SBT index
${DNASBT_BIN_PREFIX}dnasbt-text-prepare --in "$DATA"/reference/fasta \
                                        --out "$DB" \
                                        --size 4 \
                                        --names

${DNASBT_BIN_PREFIX}dnasbt-suff-extract --out "$DB"

${DNASBT_BIN_PREFIX}dnasbt-index-create --out "$DB" \
                                        --limit 2

${DNASBT_BIN_PREFIX}dnasbt-index-pack --out "$DB"

../bin/coriolis-db --tree "$DATA"/reference/nodes.dmp \
                   --taxid "$DATA"/reference/example.accession2taxid \
                   --names "$DATA"/reference/names.dmp \
                   --csbt "$DB" \
                   --out "$DB"

# now we run classifier
../bin/coriolis --csbt "$DB" --db "$DB" --in "$DATA"/reads/ --threads 4 --out nodes.result -r 5 -m 5 -l 1 -c 1 --skip-factor 0.25
cat nodes.result | ../bin/coriolis-fmt "$DB" color
