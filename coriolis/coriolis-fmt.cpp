/***
 *  $Id$
 **
 *  File: coriolis-fmt.cpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright 2019-2023 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <bio/ncbi.hpp>
#include <jaz/string.hpp>

#include <smarten/Logger.hpp>
#include <smarten/config.hpp>

#include "config.hpp"

#include <iostream>


int main(int argc, char* argv[]) {
    smrt::Logger::init();
    auto log = spdlog::stdout_color_st("CoriolisFMT");

    if (argc < 2) {
        log->info("CoriolisFMT SMARTEn {}", SMARTEN_VERSION);
        log->error("reference database not specified");
        return -1;
    }

    bool color = false;
    if (argc == 3) color = true;

    bio::NCBITaxa taxa_tree;
    std::string db_name = argv[1];

    if (!taxa_tree.restore(CORIOLIS_TREE(db_name), true)) {
        log->error("unable to restore taxonomic tree");
        return -1;
    }

    if (!taxa_tree.restore_names(CORIOLIS_NAMES(db_name))) {
        log->error("unable to restore taxonomic names");
        return -1;
    }

    int line = 0;

    std::string s;
    std::vector<std::string> v;

    std::string NORMAL{"\033[0;39m"};
    std::string RED{"\033[31m"};
    std::string GREEN{"\033[32m"};
    std::string PINK{"\033[35m"};

    if (color == false) {
        NORMAL = "";
        RED = "";
        GREEN = "";
        PINK = "";
    }

    std::string trank = "";

    while (std::getline(std::cin, s)) {
        jaz::split('\t', s, std::back_inserter(v));

        if (v.size() != 3) {
            log->warn("line {} incorrect: {} tokens", line, v.size());
        } else {
            if (v[0] == "U") {
                std::cout << v[1] << RED << "\tunclassified" << NORMAL << std::endl;
            } else {
                int id = taxa_tree.inner_id(std::stoi(v[2]));
                trank = taxa_tree.rank2name(taxa_tree.code(id));

                std::cout << v[1] << "\t"
                          << taxa_tree.name(id) << "\t";

                if (trank == "no rank") std::cout << PINK; else std::cout << GREEN;

                std::cout << trank << NORMAL << std::endl;
            }
        }

        s.clear();
        v.clear();

        line++;
    } // while

    return 0;
} // main
