/***
 *  $Id$
 **
 *  File: config.hpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright 2019-2023 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <string>

inline std::string CORIOLIS_TREE(std::string s) { return s + ".cort"; }
inline std::string CORIOLIS_NAMES(std::string s) { return s + ".corn"; }
inline std::string CORIOLIS_DB(std::string s) { return s + ".cord"; };

#endif // CONFIG_HPP
