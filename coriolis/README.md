# Coriolis Classifier

Coriolis is a fully functional metagenomic classifier designed for lightweight mobile devices, such as ONT's MinIT or the latest MinION sequencers. Similar to Centrifuge, Coriolis's classification algorithm is based on exact-match queries of arbitrary lengths. Through the use of optimized external memory data structures, Coriolis performs optimally even in extremely low-memory environments. Coriolis is designed to work with mobile devices (it remains reasonably efficient on large servers as well!), and supports both Intel and ARM processors. It generates outputs that are 100% compatible with Centrifuge, and hence can be used as a drop-in replacement in the existing workflows.

This project has been supported by [NSF](https://www.nsf.gov/) under the award [CNS-1910193](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1910193).

## Table of Contents
[[_TOC_]]

## Requirements
 * A C++20-compliant compiler, e.g., `g++-13`.
 * CMake version 3.12 or higher.
 * Boost version 1.71 or higher (for the Boost.Iostreams module).
 * The LZ4 library version 1.9.3 or higher.
 * The [spdlog](https://github.com/gabime/spdlog) library version 1.5 or higher.
 * The [DNAsbt](https://gitlab.com/SCoRe-Group/dnasbt) library version 0.1.x.
 * The Intel TBB library (if using coriolis versions prior to 0.2.x).
 * We currently support only Linux.

## Installation

To simplify the installation process, we start from listing the Debian/Ubuntu packages that are dependencies for `coriolis`.
Below we give a command line that will install the required packages. Assuming a relatively new OS release, e.g., Ubuntu Focal, the installed packages should meet the required versions.
This also applies to the NVIDIA JetPack SDK 5.x.

```
sudo apt install cmake g++ libspdlog-dev libtbb-dev libboost-all-dev liblz4-dev
```

To build `coriolis`, you first have to install [the `DNAsbt` library](https://gitlab.com/SCoRe-Group/dnasbt/). Next, download the desired release of SMARTEn from the [Releases](https://gitlab.com/SCoRe-Group/smarten/-/releases) page, e.g.,

```
git clone https://gitlab.com/SCoRe-Group/smarten.git
```

Then, `cd` into the `coriolis/` directory. Assuming that `DNAsbt` is available on your system, to start the build process simply run `./build.sh`. If `DNAsbt` is not installed in a standard location, make sure to use the `-S` switch when calling `./build.sh` to indicate where `DNAsbt` is installed. For example:

```
CXX=g++ ./build.sh -S ../../DNAsbt/release
```

will build `coriolis` using the GNU C++ compiler and the `DNAsbt` installation at `../../DNAsbt/release`. Alternatively, you can set the `DnaSbt_ROOT` environment variable to the absolute path of the directory where `DNAsbt` has been installed.

## Getting Started

### Testing

To test `coriolis`, enter the `example/` directory and call `run.sh`. Please note that if the `DNAsbt` tools are not in your `PATH`, you can use the `-S` switch to point to the directory where `DNAsbt` has been installed (or you can edit `run.sh` accordingly).

### Building Reference Database

A `coriolis` database consists of six files: `*.cord`, `*.corn`, `*.cort`, `*.dnasbt_text`, `*.dnasbt_pcsbt`, and `*.dnasbt_dsuf64`. Constructing the database involves first constructing the `DNAsbt` index of your reference FASTA sequences, and then combining that index with the NCBI provided taxonomy data. Below we give step-by-step instructions.

1. Prepare your input data. This means reference sequences in the FASTA format, as well as the following NCBI taxonomy data:
   * `nodes.dmp` with NCBI taxonomic tree,
   * `names.dmp` with NCBI taxonomic names,
   * `accession2taxid` with mapping between sequence id and taxonomic id.

   For convenience, in the `tools/` directory, we provide the `ncbi-download.sh` script that simplifies reference data acquisition. For example, to prepare data with any assembly level viral sequences from the NCBI RefSeq repository you can run:

   ```
   ncbi-download.sh -o mydata/ taxonomy
   ncbi-download.sh -d viral -a Any -t 4 -o mydata/ refseq
   ```

   This will create a `mydata/` directory with two sub-directories: `taxonomy/` containing the NCBI taxonomy, and `viral/` containing the reference sequences. Additionally, the file `mydata/viral.seqid2taxid` containing a sequence id to taxonomy id map will be generated. Please run `ncbi-download.sh` without any switches to get a list of all available options.

   Note that when downloading the reference data, we do not perform any masking. If you believe masking may help your data, you must take care of that separately.

   We also recommend running `ncbi-download-check.sh`, available from the `tools/` directory, to make sure that your downloaded data is consistent (since NCBI repositories are updated frequently, it sometimes happens that some downloads either fail or retrieve references that should not be there).

2. The next step is to construct a `DNAsbt` index of the prepared reference sequences. The [DNAsbt documentation](https://gitlab.com/SCoRe-Group/dnasbt) gives detailed instructions. To index our example viral data, you would run:

   ```
   dnasbt-text-prepare -i mydata/viral/ -o viral -n
   dnasbt-suff-extract -o viral
   dnasbt-index-create -o viral -m
   dnasbt-index-pack -o viral
   ```

   Please note that at this stage we want to keep all files created by the `DNAsbt` indexing tools. Also, when calling `dnasbt-text-prepare` we are passing `-n` option to create a mapping between the names and the ids of the input reference sequences.

3. The final step is to combine the indexed reference sequences with the NCBI taxonomic data. We provide the `coriolis-db` tool to take care of this step. Back again to the viral data, you would run:

   ```
   coriolis-db --tree mydata/taxonomy/nodes.dmp \
               --names mydata/taxonomy/names.dmp \
               --taxid mydata/viral.seqid2taxid \
               --csbt viral \
               --out viral
   ```

   Once this step is done, the final `coriolis` reference database consists of `*.cord`, `*.corn`, `*.cort`, `*.dnasbt_text`, `*.dnasbt_pcsbt`, and `*.dnasbt_dsuf64` files. In the viral data example these would be:

   ```
   viral.cord
   viral.corn
   viral.cort
   viral.dnasbt_text
   viral.dnasbt_pcsbt
   viral.dnasbt_dsuf64
   ```

   All the remaining files can be safely removed.

### Running Coriolis

With the reference database ready, running `coriolis` is as simple as calling the command:

```
coriolis --csbt db --db db --in in/ --out out.result --cache 1.25
```

This will classify all reads in a directory of FASTA files `in/` using the `coriolis` database `db` (which includes the `DNAsbt` index) while using a cache size of 1.25 GB for the database index. So to use the viral database from the example above, you would substitute `db` with `viral` as follows:

```
coriolis --csbt viral --db viral --in in/ --out out.result --cache 1.25
```

The output will be stored in the file `out.result` containing the list of all reads together with their classification. The output format is compatible with the format used by Centrifuge and Kraken tools. Note that we support several optional arguments that can be used to customize the classification process. See the usage (run `coriolis -h`) for the specifics.

## When to Use Coriolis

Coriolis is a fully functional metagenomic classifier and can be used as a drop-in replacement for other tools such as Centrifuge. Coriolis shines in environments that have little available RAM, yet have access to large and fast secondary storage (e.g., SSDs or NVRAM) on which to deploy the reference database. However, while Coriolis is designed for mobile devices, it can be deployed in any environment, including workstations, servers, and HPC centers. The only limiting factor is the speed of the storage on which you deploy your reference database.

## FAQ

>Q: I'm interested in Coriolis, but I'm not using mobile devices. Can I run Coriolis on a workstation or server?

Yes! While Coriolis is designed to be efficient in mobile settings, it is a fully functional classifier and can be deployed in any environment with sufficiently fast storage.

>Q: What block size I should use when building DNAsbt for Coriolis?

That depends on the size of your input reference data. When dealing with hundreds of GBs use block size in the range of 32KB to 64KB.
For smaller data, decreasing the block size may help with the performance.

>Q: How many cores should I use with Coriolis?

You should use enough cores/threads to saturate I/O to give the SMARTEn runtime enough flexibility to optimize the scheduling of tasks, but not so much that the threads are heavily competing for I/O. For older versions of Coriolis running in low memory setups with fast secondary storage (i.e., SSDs and NVRAM), we found 6-8 cores to work best. The Coriolis versions 0.2.x or newer usually work best with 4$\times$ the number of cores thread over-subscription. When I/O is the bottleneck, we recommend using `taskset` to reduce the number of cores utilized if your system has more than that (newer versions of Coriolis offer `--threads` switch as well). See also the next question.

>Q: How to fine-tune Coriolis performance?

To achieve a near perfect classification throughput on a given hardware platform, you can use the following protocol. First, benchmark your storage to assess its throughput. For example, using [FIO](https://fio.readthedocs.io/en/latest/):

```
fio --filename=FILE_ON_YOUR_STORAGE --rw=randread --direct=1 --bs=DNASBT_BLOCK_SIZE --ioengine=libaio --numjobs=1 --iodepth=2048 --name=speed --size=16G
```

where `FILE_ON_YOUR_STORAGE` is a file on the tested storage, and `DNASBT_BLOCK_SIZE` is the DNAsbt block size you plan to use with Coriolis.

Next, run Coriolis changing the number of threads (option `--threads`), and monitor its I/O throughput (e.g., using a tool like `htop`). Find the number of threads for which I/O throughput is close to the throughput of your storage. Coriolis is able to sustain performance close to the peak storage throughput. Use that number of threads for your production. Note that because Coriolis is I/O bound you can probably limit the number of physical cores it runs on (using `taskset`) while keeping the number of threads fixed. For example, this call

```
taskset -c 0-3 coriolis --csbt viral --db viral --in in/ --out out.result --cache 1.25 --threads 16
```

will make Coriolis to run 16 threads on 4 physical cores.

>Q: What cache size should I use with Coriolis?

When your system can't fit the entire index into memory, we found a cache size of 2 GB to be optimal. However, if you are
running on a large server and can afford to fit the whole reference database into memory, then it may be feasible to make
the cache as big as the reference database.

>Q: Why am I getting different results with Coriolis than with Centrifuge?

All modern metagenomic classifiers are based on pseudo-alignment heuristics to classify a read. Although the heuristics used by Coriolis are based on those used by Centrifuge, there are a few minor differences, and thus there may be some small discrepancies in the classification results.

>Q: Can I use Coriolis with Readfish?

While we haven't tried it ourselves yet, since Coriolis is designed to work as a drop-in replacement for Centrifuge, we expect that it can work with Readfish just fine!

## License and Copyright

© 2019-2024 [SCoRe Group](http://www.score-group.org/). SMARTEn is distributed under the terms of the Apache License 2.0.

## How to Cite

If you use `coriolis`, please cite our paper:

> A.J. Mikalsen and J. Zola. "[Coriolis: Enabling metagenomic classification on lightweight mobile devices](https://academic.oup.com/bioinformatics/article/39/Supplement_1/i66/7210430)", In: _Intelligent Systems for Molecular Biology_ (ISMB), 2023.
