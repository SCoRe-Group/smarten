/***
 *  $Id$
 **
 *  File: coriolis-db.cpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright 2019-2023 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <bio/ncbi.hpp>
#include <cxxopts/cxxopts.hpp>
#include <dnasbt/dnasbt_filenames.hpp>

#include <smarten/Logger.hpp>
#include <smarten/config.hpp>

#include "config.hpp"
#include "CoriolisDB.hpp"


int main(int argc, char* argv[]) {
    smrt::Logger::init();
    auto log = spdlog::stdout_color_mt("CoriolisDB");

    log->info("CoriolisDB SMARTEn {}", SMARTEN_VERSION);

    std::string tree_name;
    std::string taxid_name;
    std::string sbt_name;
    std::string names_name;
    std::string out_name;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("tree", "taxonomic tree in NCBI format", cxxopts::value<std::string>(tree_name))
            ("taxid", "accession to taxid mapping in NCBI format", cxxopts::value<std::string>(taxid_name))
            ("csbt", "dnasbt index path and name (<path>/<name> without extensions)", cxxopts::value<std::string>(sbt_name))
            ("names", "taxonomic names in NCBI format", cxxopts::value<std::string>(names_name)->default_value(""))
            ("o,out", "directory to store database", cxxopts::value<std::string>(out_name)->default_value("./"))
            ("h,help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("tree") && opt_res.count("taxid") && opt_res.count("csbt"))) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (opt_res.unmatched().size() != 0) {
            std::cout << options.help() << std::endl;
            return 0;
        }
    } catch (const cxxopts::exceptions::exception& e) {
        log->error("{}", e.what());
        return -1;
    }


    // 1. get taxa tree
    log->info("processing taxonomic tree...");

    bio::NCBITaxa taxa_tree;

    if (!taxa_tree.read(tree_name)) {
        log->error("unable to read taxonomic tree");
        return -1;
    }

    if (!taxa_tree.store(CORIOLIS_TREE(out_name))) {
        log->error("unable to store taxonomic tree");
        return -1;
    }

    if (!names_name.empty()) {
        log->info("attaching taxonomic names...");

        if (!taxa_tree.read_names(names_name)) {
            log->error("unable to read taxonomic names");
            return -1;
        }

        if (!taxa_tree.store_names(CORIOLIS_NAMES(out_name))) {
            log->error("unable to store taxonomic names");
            return -1;
        }
    }

    log->info("taxonomic tree size: {} nodes", taxa_tree.size());

    // 2. get accession to id mapping
    //    we should accelerate this
    log->info("processing accession to taxid mapping (be patient)...");

    bio::NCBIAccession acc2taxid;

    if (!acc2taxid.read(taxid_name)) {
        log->error("unable to read taxonomic mapping");
        return -1;
    }

    log->info("accession to taxid dictionary size: {}", acc2taxid.size());

    // 3. build db from sbt files
    log->info("creating coriolis database, mapping positions in text to internal taxids...");

    CoriolisDB db{};

    int res = db.read(sbt::tnames_name(sbt_name), acc2taxid, taxa_tree);

    if (res == -1) {
        log->error("unable to read files for database construction");
        return -1;
    }

    if (res > 0) {
        log->warn("{} sequences without assigned taxid!", res);
    }

    if (!db.store(CORIOLIS_DB(out_name))) {
        log->error("unable to serialize database");
        return -1;
    }

    log->info("done!");

    return 0;
} // main
