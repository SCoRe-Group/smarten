/***
 *  $Id$
 **
 *  File: coriolis.cpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *          Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright 2019-2024 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <memory>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

#include <bio/ncbi.hpp>
#include <cxxopts/cxxopts.hpp>
#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_config.hpp>
#include <dnasbt/dnasbt_duplicate_suffixes.hpp>
#include <dnasbt/dnasbt_filenames.hpp>
#include <dnasbt/dnasbt_names_map.hpp>
#include <dnasbt/dnasbt_storage_manager.hpp>
#include <jaz/algorithm.hpp>

#include <parallel_hashmap/phmap.h>
#include <smarten/fastx_iterator.hpp>
#include <smarten/SMARTEnThreadApp.hpp>

#include "CoriolisDB.hpp"
#include "config.hpp"


namespace {

  inline void reverse_complement(std::string& P) {
      std::reverse(P.begin(), P.end());

      for (auto c = P.begin(); c != P.end(); ++c) {
          switch (*c) {
            case 'A':
                *c = 'T';
                break;
            case 'C':
                *c = 'G';
                break;
            case 'G':
                *c = 'C';
                break;
            case 'T':
                *c = 'A';
                break;
          }
      } // for c
  } // reverse_complement

} // namespace


class ReadPreprocess {
public:
    using read_id_type = std::string;
    using read_type = std::string;

    static void init(int threshold = 16) {
        read_threshold = threshold;
    } // init

    template <typename SMARTEnRuntime>
    static void run(read_id_type&& rid, read_type&& read, SMARTEnRuntime& rt) {
        if (read.size() >= read_threshold) {
            auto read_rc = read;
            reverse_complement(read_rc);

            rt.match_search(rid, 1, std::move(read));
            rt.match_search(rid, -1, std::move(read_rc));
        } else {
            rt.log()->warn("{} too short, ignoring...", rid);
            rt.output(rid, 0);
            return;
        }
    } // run

    static inline int read_threshold = 16;

}; // class ReadPreprocess


template<typename T>
std::vector<T> load_into_vector(const std::string& filename) {
    auto size = std::filesystem::file_size(filename);

    std::vector<T> data(size / sizeof(T));
    std::ifstream f(filename);

    if (!f) {
        throw std::runtime_error("unable to open file " + filename);
    }

    f.read(reinterpret_cast<char*>(data.data()), size);

    if (!f) {
        throw std::runtime_error("unable to read from file " + filename);
    }

    f.close();

    return data;
} // load_into_vector


template <int SbtPageSize = DNASBT_PAGE_SIZE>
class MatchSearch {
public:
    static constexpr auto sbt_page_size = SbtPageSize;
    static constexpr auto b = sbt::csbtree_suffixes_per_node<sbt_page_size>();

    using query_id_type = int;
    using query_type = std::string;
    using query_result_type = int;

    using index_type = sbt::CSBTree<b, sbt::mmap_storage_manager,
                                       sbt::lz4_direct_io_storage_manager,
                                       sbt::mmap_storage_manager,
                                       sbt::basic_memory_manager,
                                       sbt::tree_memory_manager,
                                       sbt::basic_memory_manager>;

    template<typename Logger>
    static bool init(const std::string& name, Logger log, int threshold = 16, double index_cache_size = 1) {
        match_threshold = threshold;

        index_nodes = (1ULL * 1024 * 1024 * 1024 * index_cache_size) / DNASBT_PAGE_SIZE;
        bool packed = true;

        if (!sbt::csbtree_validate(sbt::pcsbt_name(name), sbt::info_name(name), SbtPageSize, packed, log)) {
            return false;
        }

        return index.load({sbt::text_name(name)}, {index_nodes, sbt::pcsbt_name(name)}, {sbt::dsuf_name(name)});
    } // init

    template <typename SMARTEnRuntime>
    static void run(const std::string& rid, const query_id_type& qid, const query_type& query, SMARTEnRuntime& rt) {
        // track if at least one significant match is found in the read
        bool has_match = false;

        // reuse a single buffer for occurrences reported by multiple queries (cleared before each query)
        std::vector<std::size_t> occ_buffer{};

        // query all substrings read[pos..len) longer than 15bp
        std::size_t pos = 0;
        auto len = query.length();

        while (len >= pos + match_threshold) {
            std::string_view substr{query.c_str() + pos, len - pos};

            // like centrifuge, we ignore matches of length less than the match threshold (i.e., insignificant matches)
            auto lcp = index.find(substr, std::back_inserter(occ_buffer), match_threshold);

            // check if significant match was found
            if (occ_buffer.size() > 0) {
                has_match = true;
            }

            // create task for each occurrence (if the match is insignificant the buffer will be empty)
            for (auto occ : occ_buffer) {
                // output +lcp for forward strand and -lcp for reverse complement
                // (recall that qid = {+1 if forward, -1 if reverse complement})
                rt.match_extend(rid, -1, {qid * lcp, occ});
            }

            // skip by the lcp factor
            // +1 is to stay back compatible
            // if skip_factor == 1 we get centrifuge-like heuristic
            occ_buffer.clear();
            pos += (static_cast<int>(skip_factor * lcp) + 1);
        } // while pos

        // if the read had no significant matches then it's unclassified
        if (!has_match) {
            // note that this must be label_assign and not output since the reverse complement may have matches
            rt.label_assign(rid, {0, 0});
        }
    } // run

    static inline std::vector<char> text;
    static inline std::vector<sbt::offset_type> dsuf;
    static inline index_type index;

    static inline int match_threshold = 16;
    static inline double skip_factor = 1.0;

    static inline std::size_t text_nodes = 0;
    static inline std::size_t index_nodes = 0;

}; // class MatchSearch


class MatchExtend {
public:
    using query_id_type = int;
    using query_type = std::tuple<int, std::size_t>;

    // emit pairs of the form (lcp, taxid)
    using query_result_type = std::pair<int, int>;

    static bool init(const std::string& sbt_name, const std::string& db_name) {
        return db.restore(CORIOLIS_DB(db_name));
    }

    template <typename SMARTEnRuntime>
    static void run(const std::string& rid, const query_id_type&, const query_type& query, SMARTEnRuntime& rt) {
        // find corresponding taxid of matched position
        rt.label_assign(rid, {std::get<0>(query), db[std::get<1>(query)]});
    } // run

    static inline CoriolisDB db;

}; // class MatchExtend


class LabelAssign {
public:
    using result_type = int;

    static bool init(const std::string& name, int threshold = 5) {
        label_threshold = threshold;
        return taxa_tree.restore(CORIOLIS_TREE(name));
    } // init

    template <typename QueryResultIterator, typename SMARTEnRuntime>
    static void run(const std::string& rid, QueryResultIterator first, QueryResultIterator last, SMARTEnRuntime& rt) {

        phmap::flat_hash_map<int, int> forward_scores{}; // scores on the forward strand
        phmap::flat_hash_map<int, int> reverse_scores{}; // scores on the reverse complement

        // compute scores for each species, scoring forward strand and reverse complement matches seperately
        for (; first != last; ++first) {
            auto& [lcp, taxid] = *first;

            // we decide which score to update based on if the match used the forward strand or the reverse complement
            // recall that we make lcp negative for the reverse complement of the read
            auto& scores = lcp > 0 ? forward_scores : reverse_scores;

            // use our scoring function to update the score for the matched species
            auto s = m_compute_score__(std::abs(lcp));
            scores[taxid] += s;
        }

        // pick best score (forward vs reverse complement) for each taxid
        // we store triplet (taxid, _, _) the last two components are arbitrary until later in the algorithm
        std::vector<std::tuple<int, int, int>> matches{};
        int best_score = 0;

        // add species unique to the forward strand along with the best scores of species in both strands
        for (auto iter = forward_scores.begin(); iter != forward_scores.end(); ++iter) {

            auto rev_elem = reverse_scores.find(std::get<0>(*iter));
            auto [taxid, score] = *iter;

            // if reverse complement has better score, update
            if (rev_elem != reverse_scores.end()) {
                if (score < std::get<1>(*rev_elem)) score = std::get<1>(*rev_elem);
                reverse_scores.erase(rev_elem);
            }

            m_track_best_matches__(taxid, score, matches, best_score);
        }

        // add species unique to the reverse complement
        for (auto iter = reverse_scores.begin(); iter != reverse_scores.end(); ++iter) {
            auto& [taxid, score] = *iter;
            m_track_best_matches__(taxid, score, matches, best_score);
        }

        // if we have more than the specified number of assignments,
        // we must traverse the taxonomic tree to reduce the number
        if (matches.size() > label_threshold) {
            std::sort(matches.begin(), matches.end());

            // encode sequences of siblings as triplets (parent, start, size)
            // observe that matches already follows a similar encoding,
            // where parent is the taxid and start, size are undefined
            std::vector<std::tuple<int, int, int>> encoding{};
            m_encode_matches__(matches, encoding);

            // encode entire level until able to reduce within threshold
            while (encoding.size() > label_threshold) {
                matches = std::move(encoding);
                m_encode_matches__(matches, encoding);
            } // while encoding

            // merge largest groups of siblings into their parents until within threshold
            std::sort(encoding.begin(), encoding.end(), m_taxid_encoding_comp_descending_size__);

            auto count = matches.size();

            for (auto& e : encoding) {
                if (count > label_threshold) {
                    // output parent of siblings
                    count -= std::get<2>(e); // note that underflow is impossible since sum of counts is matches.size()
                    rt.output(rid, taxa_tree.ncbi_id(std::get<0>(e)));
                } else {
                    // output siblings (by following pointers into the previous level's encoding)
                    auto& [_, start, size] = e;

                    for (auto j = start; j < start + size; ++j) {
                        rt.output(rid, taxa_tree.ncbi_id(std::get<0>(matches[j])));
                    }
                }
            } // for e
        } else {
            for (auto& m : matches) rt.output(rid, taxa_tree.ncbi_id(std::get<0>(m)));
        }
    } // run

    static inline bio::NCBITaxa taxa_tree;
    static inline int label_threshold = 5;


private:
    static int m_compute_score__(int lcp) {
        // we implement the same scoring function used by Centrifuge
        auto diff = lcp - 15;
        return diff * diff;
    } // m_compute_score__

    // tracks all equal matches with the highest score encountered so far
    static void m_track_best_matches__(int taxid, int score,
                                       std::vector<std::tuple<int, int, int>>& matches,
                                       int& best_score) {
        if (score == best_score) {
            matches.push_back({taxid, -1, -1});
        } else if (best_score < score) {
            matches.clear();
            matches.push_back({taxid, -1, -1});
            best_score = score;
        }
    } // m_track_best_matches__

    static bool m_taxid_encoding_is_sibling__(std::tuple<int, int, int> a, std::tuple<int, int, int> b) {
        return taxa_tree.parent(std::get<0>(a)) == taxa_tree.parent(std::get<0>(b));
    } // m_taxid_encoding_is_sibling__

    // comparison function for sorting taxid encoding on descending order
    // by the number of siblings the encoding represents
    static bool m_taxid_encoding_comp_descending_size__(std::tuple<int, int, int> a, std::tuple<int, int, int> b) {
        return !(std::get<2>(a) < std::get<2>(b));
    } // m_taxid_encoding_comp_descending_size__

    static void m_encode_matches__(const std::vector<std::tuple<int, int, int>>& matches,
                                   std::vector<std::tuple<int, int, int>>& encoding) {
        encoding.clear();

        auto iter = matches.begin();
        auto end = matches.end();

        while (iter != end) {
            auto tmp = jaz::range(iter, end, m_taxid_encoding_is_sibling__);
            encoding.push_back({taxa_tree.parent(std::get<0>(*iter)), iter - matches.begin(), tmp - iter});
            iter = tmp;
        }
    } // m_encode_matches__

}; // class LabelAssign


// we provide specialization to output results
namespace std {
  inline std::ostream& operator<<(std::ostream& os, const std::pair<std::string, int>& obj) {
      if (obj.second == 0) return (os << "U\t" << obj.first << "\t0" << "\n");
      return (os << "C\t" << obj.first << "\t" << obj.second << "\n" << std::flush);
  } // operator<<
} // namespace std


int main(int argc, char* argv[]) {
    // let's build app and run
    auto constexpr page_size = DNASBT_PAGE_SIZE;

    std::string sbt_path;
    std::string db_path;
    std::string in_dir;
    std::string output;
    bool fastq;
    bool watch;
    int watch_timeout;
    bool short_name;
    int read_size;
    int match_size;
    int max_assignments;
    double index_cache_size;
    double big_skip_factor;
    int threads = 4 * std::thread::hardware_concurrency();

    smrt::SMARTEnThreadApp<ReadPreprocess, MatchSearch<DNASBT_PAGE_SIZE>, MatchExtend, LabelAssign> app;
    app.log()->info("Coriolis SMARTEn {}", SMARTEN_VERSION);

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("s,csbt", "packed dnasbt index path and name (<path>/<name> without extensions)", cxxopts::value<std::string>(sbt_path))
            ("d,db", "coriolis database path and name (<path>/<name> without extensions)", cxxopts::value<std::string>(db_path))
            ("i,in", "file or directory containing input reads", cxxopts::value<std::string>(in_dir))
            ("o,out", "file to output classification results", cxxopts::value<std::string>(output))
            ("c,cache", "number of GB to cache index nodes in memory", cxxopts::value<double>(index_cache_size))
            ("q,fastq", "input reads in FASTQ format", cxxopts::value<bool>(fastq)->default_value("false"))
            ("w,watch", "watch input directory for streamed reads", cxxopts::value<bool>(watch)->default_value("false"))
            ("t,watch-timeout", "idle time to stop watching for input", cxxopts::value<int>(watch_timeout)->default_value("90"))
            ("x,short-names", "use short read names", cxxopts::value<bool>(short_name)->default_value("true"))
            ("r,read-size", "minimum size of a read to be classified", cxxopts::value<int>(read_size)->default_value("22"))
            ("m,match-size", "minimum length of a match to be considered significant", cxxopts::value<int>(match_size)->default_value("22"))
            ("skip-factor", "skipping heuristic factor", cxxopts::value<double>(big_skip_factor)->default_value("1.0"))
            ("l,labels", "maximum number of labels to assign to a read", cxxopts::value<int>(max_assignments)->default_value("5"))
            ("T,threads", "number of threads to use", cxxopts::value<int>(threads)->default_value(std::to_string(threads)))
            ("h,help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("csbt") && opt_res.count("db") && opt_res.count("in") && opt_res.count("out")
              && opt_res.count("cache"))) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (opt_res.unmatched().size() != 0) {
            std::cout << options.help() << std::endl;
            return 0;
        }
    } catch (const cxxopts::exceptions::exception& e) {
        app.log()->error("{}", e.what());
        return -1;
    }

    if (read_size < match_size) {
        app.log()->error("unable to use reads shorter than the minimum significant match!");
        return -1;
    }

    // set number of threads (specific to SMARTEnThreadApp)
    app.setp(threads);

    // here are our building blocks
    app.log()->info("initializing read preprocess...");
    ReadPreprocess::init(read_size);

    app.log()->info("initializing read match...");

    if (big_skip_factor <= 0.05) {
        app.log()->error("skip factor too small!");
        return -1;
    }

    MatchSearch<page_size>::skip_factor = big_skip_factor;

    if (!MatchSearch<page_size>::init(sbt_path, app.log(), match_size, index_cache_size)) {
        app.log()->error("failed to initialize!");
        return -1;
    }

    app.log()->info("initializing read extend...");
    if (!MatchExtend::init(sbt_path, db_path)) {
        app.log()->error("failed to initialize!");
        return -1;
    }

    app.log()->info("initializing label assign...");
    if (!LabelAssign::init(db_path, max_assignments)) {
        app.log()->error("failed to initialize!");
        return -1;
    }

    // here is out output iterator
    std::ofstream of(output);
    if (!of) {
        app.log()->error("failed to initialize output!");
        return -1;
    }

    std::ostream_iterator<std::pair<std::string, int>> out(of);

    // here we execute
    app.log()->info("ready to run!");


    // here we go!
    if (fastq) {
        if (watch) {
            app.log()->debug("watching {} for streamed FASTQ reads...", in_dir);
            smrt::fastx_inotify_iterator<smrt::fastq_input_iterator<false>> fiter(in_dir, watch_timeout, short_name), end;
            app.execute(fiter, end, out);
        } else {
            app.log()->debug("extracting FASTQ reads from {}", in_dir);
            smrt::fastx_files_iterator<smrt::fastq_input_iterator<false>> fiter(in_dir, short_name), end;
            app.execute(fiter, end, out);
        }
    } else {
        if (watch) {
            app.log()->debug("watching {} for streamed FASTA reads...", in_dir);
            smrt::fastx_inotify_iterator<smrt::fasta_input_iterator> fiter(in_dir, watch_timeout, short_name), end;
            app.execute(fiter, end, out);
        } else {
            app.log()->debug("extracting FASTA reads from {}", in_dir);
            smrt::fastx_files_iterator<smrt::fasta_input_iterator> fiter(in_dir, short_name), end;
            app.execute(fiter, end, out);
        }
    }

    app.log()->info("bye!");

    return 0;
} // main
