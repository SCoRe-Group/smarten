#!/bin/bash

if [ -d build/ ]; then
  cd build/
  make -j 8 install
fi
