/***
 *  $Id$
 **
 *  File: CoriolisDB.hpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright 2019-2023 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SMARTEN_CORIOLIS_DB_HPP
#define SMARTEN_CORIOLIS_DB_HPP

#include <filesystem>
#include <fstream>
#include <iterator>
#include <utility>
#include <vector>

#include <bio/ncbi.hpp>
#include <dnasbt/dnasbt_types.hpp>
#include <dnasbt/dnasbt_names_map.hpp>


/// Maps positions in text to corresponding internal taxaid.
class CoriolisDB {
public:

    /// Constructs database from sbt names map dnamap_name and accession to taxid file taxid_name.
    int read(const std::string& dnamap_name, const bio::NCBIAccession& taxid, const bio::NCBITaxa& taxa_tree) {
        sbt::dnasbt_names_map dnamap{};

        if (!dnamap.load(dnamap_name)) {
            return -1;
        }

        int um = 0;

        for (auto& elem : dnamap) {
            auto id = taxid[std::get<1>(elem)];
            if (id == 0) um++;
            db_.push_back({std::get<0>(elem), taxa_tree.inner_id(id)});
        }

        return um;
    } // read


    bool store(const std::string& name) {
        std::ofstream of{name};

        // couldn't open
        if (!of) {
            return false;
        }

        of.write(reinterpret_cast<char*>(db_.data()), db_.size() * sizeof(value_type));

        // couldn't write
        if (!of) {
            return false;
        }

        of.close();

        return true;
    } // store


    bool restore(const std::string& name) {
        std::ifstream fs{name};

        std::error_code ec{};
        auto size = std::filesystem::file_size(name, ec);

        // couldn't open
        if (!fs || ec) return false;

        db_.resize(size / sizeof(value_type));
        fs.read(reinterpret_cast<char*>(db_.data()), size);

        // couldn't read
        if (!fs) return false;

        fs.close();

        return true;
    } // restore


    int operator[](sbt::offset_type pos) {
        // Do binary search for the pair corresponding to the reference string containing the given position.
        // Note that the positions stored in db_ denote the exclusive rightmost end of the reference string.
        auto elem = std::upper_bound(db_.begin(), db_.end(), pos, m_pos_key_lt__);

        // return taxid of the pair
        return elem->val;
    } // operator[]

private:

    // represents a key-value pair of the form (pos, taxid)
    struct value_type {
        sbt::offset_type key;
        int val;
    }; // value_type


    // represent database as (pos, taxid) pairs for each reference string, stored in a vector sorted by pos
    std::vector<value_type> db_;


    // compare position to key-value pairs by their key
    static bool m_pos_key_lt__(const sbt::offset_type& pos, const value_type& kv) {
        return pos < kv.key;
    } // m_pos_key_lt__

}; // class CoriolisDB

#endif // SMARTEN_CORIOLIS_DB_HPP
