#include <iostream>
#include <set>
#include <vector>

#include <bio/ncbi.hpp>


int main(int argc, char* argv[]) {
    if (argc < 3) {
        std::cout << "usage: " << argv[0] << " accession2taxid accessions_file" << std::endl;
        return -1;
    }

    std::ifstream f(argv[2]);
    if (!f) return -1;

    bio::NCBIAccession acc2taxid;
    if (!acc2taxid.read(argv[1])) return -1;

    std::string s;
    std::string r;

    while (!f.eof()) {
        f >> s;

        if (!f) break;
        auto res = acc2taxid[s];

        if (res != 0) {
            r = s;

            auto pos = r.rfind('.');
            if (pos != std::string::npos) r = r.substr(0, pos);

            std::cout << r << "\t" << s << "\t" << res << "\t0\n";
        }
    }

    return 0;
} // main
