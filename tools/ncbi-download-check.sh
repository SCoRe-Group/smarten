#!/bin/bash

if [ -z "$1" ]; then
  echo "Usage: ncbi-download-check.sh DOMAIN"
  exit 0
fi

DIR=$1


function logger {
    DT=`date +"%Y-%m-%d %H:%M:%S"`
    echo "$DT [$1] $2"
}


if [ ! -d "./$DIR" ]; then
  logger "error" "domain <$DIR> missing in the current directory!"
  exit 1
fi

if [ ! -s "taxonomy/nodes.dmp" ]; then
  logger "error" "taxonomic tree <taxonomy/nodes.dmp> missing in the current directory!"
  exit 1
fi

REFSEQ_CAT_FIELD=5
TAXID_FIELD=6
SPECIES_TAXID_FIELD=7
VERSION_STATUS_FIELD=11
ASSEMBLY_LEVEL_FIELD=12
FTP_PATH_FIELD=20

TMP1=`mktemp`
TMP2=`mktemp`

logger "info" "checking $DIR download..."

logger "info" "testing for missing files..."

for i in `cut -f "$FTP_PATH_FIELD" $DIR/assembly_summary_filtered.txt | sed 's#\([^/]*\)$#\1/\1_genomic.fna.gz#'`; do
  NAME=`basename $i '.gz'`
  echo "$NAME" >> $TMP1
done

rm -rf $DIR.missing
touch $DIR.missing

for NAME in `cat $TMP1`; do
  if [[ ! -s $DIR/$NAME ]]; then
    echo "$NAME"
  fi
done > $DIR.missing

if [[ -s $DIR.missing ]]; then
  logger "warn" "missing files found, please check $DIR.missing!"
  logger "warn" "please consider re-downloading the database!"
else
  logger "info" "no missing files!"
fi

logger "info" "testing for extra files..."

for i in $DIR/*.fna; do
  NAME=${i##*/}
  echo "$NAME"
done > $TMP2

rm -rf $DIR.extra
touch $DIR.extra

egrep -v -f $TMP1 $TMP2 > $DIR.extra

if [[ -s $DIR.extra ]]; then
  logger "warn" "extra files found, please check $DIR.extra!"
  logger "warn" "these files should be removed!"
else
  logger "info" "no extra files found!"
fi

logger "info" "testing for taxids out of tree..."

cat $DIR.seqid2taxid | awk '{print $2'} | sort -u | awk '{print "^"$1"[[:space:]]"'} > $TMP1
egrep -o -f $TMP1 taxonomy/nodes.dmp | awk '{print $1}' | egrep -v -f - $DIR.seqid2taxid > $DIR.unknown_taxid

if [[ -s $DIR.unknown_taxid ]]; then
  logger "warn" "unknown (out of tree) taxids found, please check $DIR.unknown_taxid!"
  logger "warn" "consider updating taxonomy data!"
else
  logger "info" "no unknown taxids found!"
fi

rm -rf $TMP2 $TMP1

logger "info" "done!"
