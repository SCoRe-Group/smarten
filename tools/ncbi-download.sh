#!/bin/bash

# Large portion of this code comes from centrifuge-download.sh
# available from: https://github.com/DaehwanKimLab/centrifuge

function logger {
    DT=`date +"%Y-%m-%d %H:%M:%S"`
    echo "$DT [$1] $2"
}


export PCOUNT=0
function count {
    while read L; do
        IFS=$'\t' read -r DELIM CODE FILEPATH <<< "$L"
        if [[ "$DELIM" == "___" ]]; then
            if [[ "$CODE" == "ok" ]]; then
                PCOUNT=$(( PCOUNT + 1 ))
                if [ $(( PCOUNT % 100 )) -eq 0 ]; then
                    P="$(echo "$PCOUNT" "$N_EXPECTED" | awk '{printf "%.1f", $1 * 100 / $2}')"
                    T="$(echo "$PCOUNT" "$SECONDS" | awk '{printf "%.3f", $1 / $2}')"
                    logger "info" "completed $P%, $PCOUNT/$1, $T genomes/sec"
                fi
            else
                if [[ "$CODE" == "err0" ]]; then
                    logger "warn" "<$FILEPATH> is not a valid url, ignoring!"
                fi
                if [[ "$CODE" == "err1" ]]; then
                    logger "warn" "failed to download <$FILEPATH>, ignoring!"
                fi
                if [[ "$CODE" == "err2" ]]; then
                    logger "error" "<$FILEPATH> seems incorrect!"
                fi
                if [[ "$CODE" == "err3" ]]; then
                    logger "error" "failed unzipping <$FILEPATH>!"
                fi
            fi
        else
            echo "$L" >> $LIBDIR/$DOMAIN.seqid2taxid
        fi
    done
}


cut_after_first_space_or_second_pipe() {
    grep '^>' | sed 's/ .*//' | sed 's/\([^|]*|[^|]*\).*/\1/'
}
export -f cut_after_first_space_or_second_pipe


map_headers_to_taxid() {
    grep '^>' | cut_after_first_space_or_second_pipe | sed -e "s/^>//" -e  "s/\$/    $1/"
}
export -f map_headers_to_taxid


function download_n_process() {
    IFS=$'\t' read -r TAXID FILEPATH <<< "$1"

    if [[ ! "$FILEPATH" == http* ]]; then
        echo -e "___\terr0\t$FILEPATH"
        return
    fi

    NAME=`basename "$FILEPATH" .gz`
    RES_FILE="$LIBDIR/$DOMAIN/$NAME"

    if [[ ! -s "$RES_FILE" ]]; then
        $DL_CMD "$LIBDIR/$DOMAIN/$NAME.gz" "$FILEPATH" || {
            echo -e "___\terr1\t$FILEPATH"
            return
        }

        [[ -s "$LIBDIR/$DOMAIN/$NAME.gz" ]] || {
            echo -e "___\terr2\t$FILEPATH"
            return
        }

        gunzip -f "$LIBDIR/$DOMAIN/$NAME.gz" || {
            echo -e "___\terr3\t$FILEPATH"
            return
        }
    fi

    cat $RES_FILE | map_headers_to_taxid $TAXID
    echo -e "___\tok"
}
export -f download_n_process


function download_n_process_nofail() {
    download_n_process "$@" || true
}
export -f download_n_process_nofail



ALL_DOMAINS="bacteria, viral, archaea, fungi, protozoa, invertebrate, plant, vertebrate_mammalian, vertebrate_other"
ALL_ASSEMBLY_LEVELS="Complete Genome, Chromosome, Scaffold, Contig, Any"

FTP="ftp://ftp.ncbi.nih.gov"

DDIR=.
DATABASE="refseq"
DOMAINS="bacteria"
ASSEMBLY_LEVEL="Complete Genome"
N_PROC=2
VERBOSE=""

USAGE="Usage: $(basename $0) [options] <refseq|taxonomy>

Options:
   -o <path>        Directory to which to download (default: $DDIR)
   -t <number>      Number of threads to use (defult: $N_PROC)
   -d <domain>      Domain to download, subset of: $ALL_DOMAINS (default: $DOMAINS)
   -a <level>       Assembly level to consider, one of: $ALL_ASSEMBLY_LEVELS (default: $ASSEMBLY_LEVEL)
   -v               Be verbose (default: none)

"

if [ -z "$1" ]; then
    echo "$USAGE"
    exit 1
fi

while getopts "o:t:d:a:v" OPT "$@"; do
    case $OPT in
        o) DDIR="$OPTARG" ;;
        t) N_PROC="$OPTARG" ;;
        d) DOMAINS=${OPTARG//,/ } ;;
        a) ASSEMBLY_LEVEL="$OPTARG" ;;
        v) VERBOSE="--show-progress";;
        \?) echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
        :) echo "Option -$OPTARG requires an argument" >&2
           exit 1
           ;;
    esac
done

shift $((OPTIND - 1))

[[ $# -eq 1 ]] || { echo "$USAGE" && exit 1; }
DATABASE=$1

export LIBDIR="$DDIR"


if hash wget 2> /dev/null; then
    DL_PROG="wget"
    export DL_CMD="wget $VERBOSE -c -t 4 -N --reject=index.html -q -O"
else
    logger "error" "wget command missing!"
    exit 1
fi


if [[ "$DATABASE" == "taxonomy" ]]; then
    mkdir -p $LIBDIR/taxonomy
    cd $LIBDIR/taxonomy
    logger "info" "downloading NCBI taxonomy..."
    $DL_CMD taxdump.tar.gz $FTP/pub/taxonomy/taxdump.tar.gz
    tar -zxvf taxdump.tar.gz nodes.dmp
    tar -zxvf taxdump.tar.gz names.dmp
    rm taxdump.tar.gz
    logger "info" "downloading NCBI accession2taxid mappings..."
    $DL_CMD nucl_gb.accession2taxid.gz $FTP/pub/taxonomy/accession2taxid/nucl_gb.accession2taxid.gz
    $DL_CMD nucl_wgs.accession2taxid.gz $FTP/pub/taxonomy/accession2taxid/nucl_wgs.accession2taxid.gz
    logger "info" "unpacking mappings..."
    gzip -d nucl_gb.accession2taxid.gz
    gzip -d nucl_wgs.accession2taxid.gz
    cd -
    logger info "done!"
    exit 0
fi

if [[ "$DATABASE" == "refseq" ]]; then
    # fields in assembly_summary.txt
    REFSEQ_CAT_FIELD=5
    TAXID_FIELD=6
    SPECIES_TAXID_FIELD=7
    VERSION_STATUS_FIELD=11
    ASSEMBLY_LEVEL_FIELD=12
    FTP_PATH_FIELD=20

    AWK_QUERY="\$$VERSION_STATUS_FIELD==\"latest\""
    [[ "$ASSEMBLY_LEVEL" != "Any" ]] && AWK_QUERY="$AWK_QUERY && \$$ASSEMBLY_LEVEL_FIELD==\"$ASSEMBLY_LEVEL\""

    logger "info" "requested domains: $DOMAINS..."
    logger "info" "using $N_PROC threads..."

    for DOMAIN in $DOMAINS; do
        export DOMAIN=$DOMAIN
        mkdir -p $LIBDIR/$DOMAIN

        FULL_ASSEMBLY_SUMMARY_FILE="$LIBDIR/$DOMAIN/assembly_summary.txt"
        ASSEMBLY_SUMMARY_FILE="$LIBDIR/$DOMAIN/assembly_summary_filtered.txt"

        logger "info" "downloading ftp://ftp.ncbi.nlm.nih.gov/genomes/$DATABASE/$DOMAIN/assembly_summary.txt..."

        $DL_CMD "$FULL_ASSEMBLY_SUMMARY_FILE" ftp://ftp.ncbi.nlm.nih.gov/genomes/$DATABASE/$DOMAIN/assembly_summary.txt > "$FULL_ASSEMBLY_SUMMARY_FILE" || {
            logger "error" "download failed!"
            exit 1
        }

        logger "info" "filtering assembly_summary.txt"

        awk -F "\t" "BEGIN {OFS=\"\t\"} $AWK_QUERY" "$FULL_ASSEMBLY_SUMMARY_FILE" > "$ASSEMBLY_SUMMARY_FILE"

        N_EXPECTED=`cat "$ASSEMBLY_SUMMARY_FILE" | wc -l`
        export N_EXPECTED=$((N_EXPECTED))
        [[ $N_EXPECTED -gt 0 ]] || {
            logger "error" "no genomes found!"
            exit 1
        }

        logger "info" "downloading $N_EXPECTED $DOMAIN genomes, assembly level: $ASSEMBLY_LEVEL..."

        rm -rf $LIBDIR/$DOMAIN.seqid2taxid

        SECONDS=1
        cut -f "$TAXID_FIELD,$FTP_PATH_FIELD" "$ASSEMBLY_SUMMARY_FILE" | sed 's#\([^/]*\)$#\1/\1_genomic.fna.gz#' | \
            tr '\n' '\0' | xargs -0 -P $N_PROC -I {} /bin/bash -c 'download_n_process_nofail "$@"' _ {} | count $N_EXPECTED
    done

    logger "info" "completed 100%"
    logger "info" "done!"

    exit 0
fi

echo "$USAGE"
