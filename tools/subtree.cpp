#include <iostream>
#include <set>
#include <vector>

#include <bio/ncbi.hpp>


int main(int argc, char* argv[]) {
    if (argc < 3) {
        std::cout << "usage: " << argv[0] << " tree taxid_list" << std::endl;
        return -1;
    }

    bio::NCBITaxa tree;
    if (!tree.read(argv[1])) return -1;

    std::set<int> nodes;
    std::vector<int> path;

    std::ifstream f(argv[2]);
    if (!f) return -1;

    std::string buf;

    while (!f.eof()) {
        buf = "";
        std::getline(f, buf);
        if (buf.empty()) continue;
        path.clear();
        tree.path(tree.inner_id(std::stoi(buf)), path);
        for (auto x : path) nodes.insert(x);
    }

    tree.print(nodes.begin(), nodes.end());

    return 0;
} // main
