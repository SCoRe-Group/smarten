#include <fstream>
#include <iostream>

#include <fastx_iterator.hpp>


int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cout << "usage: " << argv[0] << " fastq_file fasta_file" << std::endl;
        return -1;
    }

    std::ifstream f(argv[1]);
    if (!f) {
        std::cout << "error: could not open fastq file" << std::endl;
        return -1;
    }

    std::ofstream of(argv[2]);
    if (!of) {
        std::cout << "error: could not create fasta file" << std::endl;
        return -1;
    }

    smrt::fastq_input_iterator first(f), last;

    for (; first != last; ++first) {
        auto& [name, s, qs] = *first;
        of << ">" << name << std::endl;
        of << s << std::endl;
    }

    return 0;
} // main
