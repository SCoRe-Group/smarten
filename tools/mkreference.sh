#!/bin/bash

# path to dir with NCBI taxonomy containing:
# names.dmp
# nodes.dmp
# nucl_gb.accession2taxid
# nucl_wgs.accession2taxid
# nucl_extra.accession2taxid
if [ -z "$NCBI_TAXONOMY_DIR" ]; then
  NCBI_TAXONOMY_DIR=./taxonomy
fi

# path to SMARTEn tools
if [ -z "$SMARTEN_TOOLS" ]; then
  SMARTEN_TOOLS=./tools
fi

if [ -z "$2" ]; then
  echo "usage: $0 fasta_dir fasta_ext"
  exit 0
fi

echo "using NCBI_TAXONOMY_DIR=$NCBI_TAXONOMY_DIR"
echo "using SMARTEN_TOOLS=$SMARTEN_TOOLS"

if [ `readlink -f $PWD` == `readlink -f $NCBI_TAXONOMY_DIR` ]; then
  echo "working directory must be different than NCBI_TAXONOMY_DIR"
  exit 0
fi


# extract reference sequence ids
echo "extracting reference ids..."
for i in "$1"/*.$2; do
  egrep "^>" $i | awk '{print $1}' | sed -e 's/>//'
done > _names

# extract taxids
echo "extracting taxids..."
$SMARTEN_TOOLS/acc2taxid $NCBI_TAXONOMY_DIR/nucl_gb.accession2taxid _names > _ids0
$SMARTEN_TOOLS/acc2taxid $NCBI_TAXONOMY_DIR/nucl_wgs.accession2taxid _names > _ids1

if [ -f $NCBI_TAXONOMY_DIR/nucl_extra.accession2taxid ]; then
  $SMARTEN_TOOLS/acc2taxid $NCBI_TAXONOMY_DIR/nucl_extra.accession2taxid _names > _ids2
fi

OUT=new.accession2taxid
head -n 1 $NCBI_TAXONOMY_DIR/nucl_gb.accession2taxid > $OUT
cat _ids0 >> $OUT
cat _ids1 >> $OUT
if [ -f _ids2 ]; then
  cat _ids2 >> $OUT
fi

# extract subtree
echo "extracting subtree..."
cat $OUT | awk '{print $3}' | grep -v taxid > _ids3
$SMARTEN_TOOLS/subtree $NCBI_TAXONOMY_DIR/nodes.dmp _ids3 > nodes.dmp

# extract names
echo "extracting names..."
cat nodes.dmp | awk '{print $1}' > _ids4
for i in `cat _ids4`; do egrep ^$i[[:space:]] $NCBI_TAXONOMY_DIR/names.dmp; done > names.dmp

# cleanup
echo "cleaning..."
rm -f _names _ids0 _ids1 _ids2 _ids3 _ids4
