#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <string>

inline std::string KRAKIT_TREE(std::string s) { return s + ".kt"; }
inline std::string KRAKIT_NAMES(std::string s) { return s + ".kn"; }
inline std::string KRAKIT_READS(std::string s) { return s; }

#endif // CONFIG_HPP
