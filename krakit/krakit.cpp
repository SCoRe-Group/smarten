#include <cstdlib>
#include <exception>
#include <filesystem>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

#include <bio/ncbi.hpp>
#include <cxxopts/cxxopts.hpp>

#include <parallel_hashmap/phmap.h>
#include <smarten/fastx_iterator.hpp>
#include <smarten/SMARTEnTBBApp.hpp>

#include "KrakItDB.hpp"
#include "config.hpp"
#include "kmer_extract.hpp"


class ReadPreprocess {
public:
    using read_id_type = std::string;
    using read_type = std::string;

    static void init() { kmer_extract::init(); }

    template <typename SMARTEnRuntime>
    static void run(read_id_type&& rid, read_type&& read, SMARTEnRuntime& rt) {
        const char* s = read.c_str();
        auto l = read.size();

        if (l < kmer_extract::k) {
            rt.log()->warn("{} too short, ignoring...", rid);
            rt.label_assign(rid, 0);
            return;
        }

        phmap::flat_hash_set<kmer_extract::value_type> K;

        int pos = 0;

        kmer_extract::value_type kmer = 0;
        kmer_extract::value_type kmer_rev = 0;

        while (kmer_extract::slide(s, pos, kmer, kmer_rev) == true) {
            auto query = std::min(kmer ^ kmer_extract::MASK, kmer_rev ^ kmer_extract::MASK);

            if (K.find(query) == std::end(K)) {
                K.insert(query);
                rt.match_search(rid, 0, query);
            }
        } // while kmer_extract
    } // run

}; // class ReadPreprocess


class MatchSearch {
public:
    using query_id_type = int;
    using query_type = kmer_extract::value_type;
    using query_result_type = int;

    static bool init(const std::string& name) {
        return db.load(KRAKIT_READS(name));
    } // init

    template <typename SMARTEnRuntime>
    static void run(const std::string& rid, query_id_type qid, query_type query, SMARTEnRuntime& rt) {
        // for given k-mer represented by query we extract LCA taxa id
        auto res = db.find(query);
        rt.label_assign(rid, res);
    } // run

    static KrakItDB db;

}; // class MatchSearch

KrakItDB MatchSearch::db;


class LabelAssign {
public:
    using result_type = int;

    static bool init(const std::string& name) {
        return T.restore(KRAKIT_TREE(name));
    } // init

    template <typename QueryResultIterator, typename SMARTEnRuntime>
    static void run(const std::string& rid, QueryResultIterator first, QueryResultIterator last, SMARTEnRuntime& rt) {
        // [first, last) goes over the list of taxa_id
        phmap::flat_hash_map<int, int> C;

        int min_id = T.size();

        for (; first != last; ++first) {
            if (*first > 0) {
                C[*first] += 1;
                min_id = std::min(min_id, *first);
            }
        }

        // min_id is either root of the induced subtree
        // or if root is not among nodes, it gives bound
        // on how far in the tree we should be looking
        int best_id = 0;
        int best_score = 0;

        for (auto iter = std::begin(C), end = std::end(C); iter != end; ++iter) {
            int u = iter->first;
            int score = iter->second;

            while (true) {
                int p = T.parent(u);
                if ((p == u) || (p < min_id)) break;

                u = p;

                auto pos = C.find(u);
                if (pos != end) score += pos->second;
            }

            // we should probably consider tie-breaking
            if (best_score < score) {
                best_id = iter->first;
                best_score = score;
            }
        } // for iter

        rt.output(rid, T.ncbi_id(best_id));
    } // run

    static bio::NCBITaxa T;

}; // class LabelAssign

bio::NCBITaxa LabelAssign::T;


// we provide specialization to output results
namespace std {
  inline std::ostream& operator<<(std::ostream& os, const std::pair<std::string, int>& obj) {
      if (obj.second == 0) return (os << "U\t" << obj.first << "\t0" << "\n");
      return (os << "C\t" << obj.first << "\t" << obj.second << "\n" << std::flush);
  } // operator<<
} // namespace std


int main(int argc, char* argv[]) {
    std::string db_dir;
    std::string in_dir;
    std::string output;
    bool fastq = false;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("d,db", "database path and name", cxxopts::value<std::string>(db_dir))
            ("i,in", "file or directory containing input reads", cxxopts::value<std::string>(in_dir))
            ("o,out", "file to output classification results", cxxopts::value<std::string>(output))
            ("q,fastq", "input reads in FASTQ format", cxxopts::value<bool>(fastq)->default_value("false"))
            ("h,help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("db") && opt_res.count("in") && opt_res.count("out"))) {
            std::cout << options.help() << std::endl;
            return 0;
        }

    } catch (const cxxopts::exceptions::exception& e) {
        std::cerr << "error: " << e.what() << std::endl;
        return -1;
    }

    // let's build app and run
    smrt::SMARTEnTBBApp<ReadPreprocess, MatchSearch, MatchSearch, LabelAssign> app;

    // here are our building blocks
    app.log()->info("initializing read preprocess...");
    ReadPreprocess::init();

    app.log()->info("initializing read match...");
    if (!MatchSearch::init(db_dir)) {
        app.log()->error("failed to initialize!");
        return -1;
    }

    app.log()->info("initializing label assign...");
    if (!LabelAssign::init(db_dir)) {
        app.log()->error("failed to initialize!");
        return -1;
    }

    // here is out output iterator
    std::ofstream of(output);
    if (!of) {
        app.log()->error("failed to initialize output!");
        return -1;
    }

    std::ostream_iterator<std::pair<std::string, int>> out(of);

    // here we execute
    app.log()->info("ready to run... :)");

    if (fastq) {
        app.log()->debug("extracting FASTQ reads from {}", in_dir);
        smrt::fastx_files_iterator<smrt::fastq_input_iterator<false>> fiter(in_dir), end;
        app.execute(fiter, end, out);
    } else {
        app.log()->debug("extracting FASTA reads from {}", in_dir);
        smrt::fastx_files_iterator<smrt::fasta_input_iterator> fiter(in_dir), end;
        app.execute(fiter, end, out);
    }

    app.log()->info("finishing...");

    return 0;
} // main
