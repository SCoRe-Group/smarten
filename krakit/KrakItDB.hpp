#ifndef KRAKITDB_HPP
#define KRAKITDB_HPP

#include <atomic>
#include <limits>
#include <string>
#include <thread>
#include <vector>

#include <bio/ncbi.hpp>

#include <tbb/concurrent_queue.h>
#include <tbb/parallel_reduce.h>
#include <tbb/task_group.h>

#include <smarten/circular_buffer.hpp>
#include <smarten/fastx_iterator.hpp>
#include <smarten/Logger.hpp>

#include "dbtune/dbtune.hpp"
#include "kmer_extract.hpp"


class KrakItDB {
public:
    using key_type = kmer_extract::value_type;
    using value_type = int;


    #ifdef WITH_BERKELEYDB
    using database_type = dbtune::berkeley_db<key_type, value_type>;
    #elif WITH_ROCKSDB
    using database_type = dbtune::rocks_db<key_type, value_type>;
    #else
    using database_type = dbtune::mem_hash_table<key_type, value_type>;
    #endif


    bool load(const std::string& name) {
        return db_.load(name);
    } // load


    value_type find(key_type& k) {
        const auto& [res, iter, val] = db_.find(k);
        return val;
    } // find


private:
    friend class KrakItDBHelper;
    database_type db_;

}; // class KrakItDB


class KrakItDBHelper {
public:
    using key_type = KrakItDB::key_type;
    using value_type = KrakItDB::value_type;


    explicit KrakItDBHelper(std::shared_ptr<spdlog::logger> log) : log_(log) { }

    // we return tuple with: size of the database, total number of processed reads,
    // number of ignored reads, and total length of processed reads
    std::tuple<int, int, int, long long int> create(const std::string& in_name,
                                                    const std::string& out_name,
                                                    const bio::NCBITaxa& taxa_tree,
                                                    const bio::NCBIAccession& acc2taxid,
                                                    bool add_all = false) {
        KrakItDB::database_type db;
        if (db.create(out_name) == false) throw(std::runtime_error("failed to create database"));

        commit_th_ = std::thread(&KrakItDBHelper::m_commit_update_task__, this, &db, &taxa_tree);

        int total = 0;
        int skip = 0;

        long long int length = 0;

        kmer_extract::init();
        const kmer_extract::value_type MAX_SKETCH = std::numeric_limits<key_type>::max();

        smrt::fastx_files_iterator<smrt::fasta_input_iterator> fiter(in_name), end;
        std::pair<int, int> pos;

        tbb::task_group tg;

        for (; fiter != end; ++fiter) {
            const auto& [id, s] = *fiter;

            length += s.size();
            total++;

            // get name (we also handle Kraken names)
            std::string name = id.substr(0, id.find(' '));
            auto rp = name.rfind('|');

            if (rp == std::string::npos) rp = 0; else ++rp;
            name = name.substr(rp);

            // get taxa id
            int tax_id = taxa_tree.inner_id(acc2taxid[name]);

            // we ignore uninformative sequences (unless instructed not to)
            if ((!add_all) && (tax_id == 0)) {
                skip++;
                continue;
            }

            log_->debug("adding {}", name);

            // buffer the input
            pos = rbuffer_.put({tax_id, s});

            // send buffer for processing
            if (pos.first != -1) {
                tg.run([self = this, p = pos.first, &taxa_tree]{ self->m_process_task__(p, taxa_tree); });
            }

        } // for fiter

        // handle leftovers
        pos = rbuffer_.put();

        if (pos.first != -1) {
            tg.run([self = this, p = pos.first, &taxa_tree]{ self->m_process_task__(p, taxa_tree); });
        }

        log_->info("waiting for final tasks...");

        tg.wait();

        q_done_.store(true);
        commit_th_.join();

        return {db.size(), total, skip, length};
    } // create


private:
    using read_type = std::tuple<int, std::string>;
    using storage_type = phmap::node_hash_map<key_type, value_type>;

    void m_process_task__(int pos, const bio::NCBITaxa& taxa_tree) {
        storage_type store;

        auto reads = rbuffer_.get(pos);

        if (reads.empty()) return;

        for (const auto& r : reads) {
            int tax_id = std::get<0>(r);
            const std::string& s = std::get<1>(r);

            kmer_extract::value_type kmer = 0;
            kmer_extract::value_type kmer_rev = 0;

            int kmer_pos = 0;
            int win_pos = 0;

            kmer_extract::value_type min_sketch = kmer_extract::MASK;
            int min_sketch_pos = -1;

            while (kmer_extract::slide(s, kmer_pos, kmer, kmer_rev) == true) {
                auto sketch = std::min(kmer ^ kmer_extract::MASK, kmer_rev ^ kmer_extract::MASK);
                auto res = store.insert({sketch, tax_id});
                if (res.second == false) res.first->second = taxa_tree.lca({tax_id, res.first->second});
            } // while kmer_extract
        } // for r

        Qets_.emplace(std::move(store));
    } // m_process_task__

    void m_commit_update__(KrakItDB::database_type& db, const bio::NCBITaxa& taxa_tree, storage_type&& store) {
        log_->debug("running commit with {} keys...", store.size());

        for (auto& x : store) {
            auto [res, iter, val] = db.find(x.first);
            if (!res) db.insert(x.first, x.second);
            else {
                auto lca = taxa_tree.lca({val, x.second});
                if (val != lca) db.update(iter, lca);
            }
        }

        store.clear();

        log_->debug("commit done!");
        log_->debug("{} remaining commits", Qets_.unsafe_size());
    } // m_commit_update__

    void m_commit_update_task__(KrakItDB::database_type* db, const bio::NCBITaxa* taxa_tree) {
        storage_type store;

        while (true) {
            auto res = Qets_.try_pop(store);
            if (res == true) {
                m_commit_update__(*db, *taxa_tree, std::move(store));
                store.clear();
            } else {
                if (q_done_.load() && Qets_.empty()) break;
            }
        } // true
    } // m_commit_update_task__


    const int GENOMES_PER_BLOCK = 8;
    smrt::circular_block_buffer<read_type, 32> rbuffer_{GENOMES_PER_BLOCK};

    tbb::concurrent_queue<storage_type> Qets_;

    std::atomic_bool q_done_ = false;
    std::thread commit_th_;

    std::shared_ptr<spdlog::logger> log_;

}; // class KrakItDBHelper

#endif // KRAKITDB_HPP
