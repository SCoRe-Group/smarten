#!/bin/bash

DATA="../../data"
DB="example-db"

# first we index reference data to create database
# note that we can skip names (--names)
#valgrind --tool=memcheck --leak-check=full --track-origins=yes --log-file=%p.valog \
#inspxe-cl -collect ti3 \
../bin/krakit-db --tree "$DATA"/reference/nodes.dmp \
                 --taxid "$DATA"/reference/example.accession2taxid \
                 --names "$DATA"/reference/names.dmp \
                 --add-all \
                 --in "$DATA"/reference/fasta \
                 --out "$DB"

# now we run classifier
#inspxe-cl -collect ti3 \
#valgrind --tool=memcheck --leak-check=full --track-origins=yes --log-file=%p.valog \
../bin/krakit --db "$DB" --in "$DATA"/reads/ --out nodes.result

# and we finalize results into Pavian-ready report
../bin/krakit-report --db "$DB" \
                     --in nodes.result \
                     --out nodes.report
