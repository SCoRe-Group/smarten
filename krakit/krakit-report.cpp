#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include <bio/ncbi.hpp>
#include <cxxopts/cxxopts.hpp>
#include <jaz/iterator.hpp>
#include <jaz/string.hpp>

#include <parallel_hashmap/phmap.h>
#include <smarten/Logger.hpp>

#include "config.hpp"


struct tree_stats {
    phmap::flat_hash_map<int, int> total;
    phmap::flat_hash_map<int, int> count;
    int sum;
}; // struct tree_stats


template <typename DTree>
void prune_tree(const bio::NCBITaxa& T, DTree& dT, tree_stats& ts, int root) {
    auto pos = std::remove_if(std::begin(dT[root]), std::end(dT[root]),
                              [&](int x) { return ts.total.find(T.ncbi_id(x)) == ts.total.end(); });

    dT[root].erase(pos, std::end(dT[root]));

    std::sort(std::begin(dT[root]), std::end(dT[root]),
              [&](int x, int y) { return ts.total[T.ncbi_id(x)] > ts.total[T.ncbi_id(y)]; });

    for (auto x : dT[root]) prune_tree(T, dT, ts, x);
} // prune_tree


inline void print_node(const bio::NCBITaxa& T, const tree_stats& ts, int root, int level, std::ostream& os) {
    int total = ts.total.find(T.ncbi_id(root))->second;
    int count = 0;

    auto pos = ts.count.find(T.ncbi_id(root));
    if (pos != ts.count.end()) count = pos->second;

    os << std::fixed << std::setprecision(3);
    os << ((1.0 * total) / ts.sum) << "\t" << total << "\t" << count << "\t";

    os << T.code(root) << "\t" << T.ncbi_id(root) << "\t";

    for (int i = 0; i < level; ++i) os << "  ";
    os << T.name(root) << std::endl;
} // print_node

template <typename DTree>
void print_tree(const bio::NCBITaxa& T, const DTree& dT, const tree_stats& ts,
                int root, int level, std::ostream& os) {
    print_node(T, ts, root, level, os);
    if (root < dT.size()) for (auto x : dT[root]) print_tree(T, dT, ts, x, level + 1, os);
} // print_tree


int main(int argc, char* argv[]) {
  smrt::Logger::init();
    auto log = spdlog::stdout_color_mt("KrakitReport");

    std::string db_name;
    std::string in_name;
    std::string out_name;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("db", "directory storing database", cxxopts::value<std::string>(db_name))
            ("in", "file with krakit output", cxxopts::value<std::string>(in_name))
            ("out", "file to store report", cxxopts::value<std::string>(out_name))
            ("help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("db") && opt_res.count("in") && opt_res.count("out"))) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (!opt_res.unmatched().empty()) {
            std::cout << options.help() << std::endl;
            return 0;
        }
    } catch (cxxopts::exceptions::exception& e) {
        log->error("{}", e.what());
        return -1;
    }


    bio::NCBITaxa T;

    log->info("reading taxonomic tree...");

    if (!T.restore(KRAKIT_TREE(db_name))) {
        log->error("unable to restore taxonomic tree!");
        return -1;
    }

    T.restore_names(KRAKIT_NAMES(db_name));


    auto dT = T.inner_tree();


    // 1. get counts and totals
    log->info("extracting clade counts...");

    std::ifstream f(in_name);

    if (!f) {
        log->error("unable to read krakit output!");
        return -1;
    }

    // ts stores stats using original, i.e., not mapped tax_id
    tree_stats ts;

    jaz::getline_iterator<> fiter(f), end;
    std::vector<std::string> buf;
    std::vector<int> p;

    int line = 0;
    std::string pat = " \t";

    int tax_id = 0;

    for (; fiter != end; ++fiter, ++line) {
        buf.clear();
        jaz::split(pat, *fiter, std::back_inserter(buf));

        if (buf.size() < 3) {
            log->error("incorrect line {} in output data", line);
            return -1;
        }

        try {
            tax_id = std::stoi(buf[2]);
        } catch (...) {
            log->error("incorrect tax_id in line {}", line);
            return -1;
        }

        T.path(T.inner_id(tax_id), p);

        for (auto x : p) ts.total[T.ncbi_id(x)]++;
        ts.count[tax_id]++;
    } // for fiter

    ts.sum = ts.total[0] + ts.total[1];

    log->info("total reads: {}, {} unclassified, {} classified", ts.sum, ts.total[0], ts.total[1]);


    // 2. compile report
    log->info("generating report...");

    std::ofstream of(out_name);

    if (!of) {
        log->error("unable to create report file!");
        return -1;
    }

    // first we prune and sort dT
    prune_tree(T, dT, ts, 1);

    // no we can print
    print_node(T, ts, 0, 0, of);
    print_tree(T, dT, ts, 1, 0, of);

    log->info("finishing...");

    return 0;
} // main
