#ifndef KMER_EXTRACT_HPP
#define KMER_EXTRACT_HPP

#include <cinttypes>
#include <string>


struct kmer_extract {
    using value_type = uint64_t;

    static void init() {
        for (int i = 0; i < 256; ++i) LUT[i] = 5;

        LUT['A'] = 0;
        LUT['C'] = 1;
        LUT['G'] = 2;
        LUT['T'] = 3;

        // set mask (this is mask to ensure correct padding of kmer_rev)
        mask = 0;
        for (int i = 0; i < k; ++i) mask |= (static_cast<value_type>(3) << (2 * i));
    } // init

    static bool find_valid_kmer(const std::string& s, int& pos, value_type& kmer, value_type& kmer_rev) {
        if ((s.size() < k) || (pos >= s.size() - k)) return false;

        int i = 0;

        kmer = 0;
        kmer_rev = 0;

        for (i = 0; i < k; ++i, ++pos) {
            auto c = LUT[s[pos]];

            if (c == 5) {
                if (pos >= s.size() - k) return false;
                i = 0;
                pos++;
                kmer = 0;
                kmer_rev = 0;
                continue;
            }

            kmer |= (static_cast<value_type>(c) << (2 * i));
            kmer_rev |= (static_cast<value_type>(3 - c) << (2 * (k - i - 1)));
        } // for i

        return true;
    } // find_valid_kmer

    static bool slide(const std::string& s, int& pos, value_type& kmer, value_type& kmer_rev) {
        int n = s.size();
        if (pos >= n) return false;

        if (pos == 0) return find_valid_kmer(s, pos, kmer, kmer_rev);
        else {
            auto c = LUT[s[pos]];
            pos++;

            if (c == 5) return find_valid_kmer(s, pos, kmer, kmer_rev);

            kmer >>= 2;
            kmer |= (static_cast<value_type>(c) << (2 * k - 2));

            kmer_rev <<= 2;
            kmer_rev &= mask;
            kmer_rev |= static_cast<value_type>(3 - c);
        }

        return true;
    } // slide

    inline static const int k = 31; // we use 31 to avoid palindromes

    // randomization mask (large prime, selected randomly)
    static const kmer_extract::value_type MASK = 0x89588122F9A6A6C5;
    // static const kmer_extract::value_type MASK = 0x5C76721D;
    // static const kmer_extract::value_type MASK = 0x3A31;


    inline static value_type mask = 0;
    inline static uint8_t LUT[256];

}; // struct kmer_extract

#endif // KMER_EXTRACT_HPP
