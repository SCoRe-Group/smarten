#ifndef DBTUNE_HPP
#define DBTUNE_HPP

#include <filesystem>
#include <fstream>
#include <string>
#include <vector>

#include <parallel_hashmap/phmap.h>
#include <parallel_hashmap/phmap_dump.h>

#ifdef WITH_ROCKSDB
#include <rocksdb/db.h>
#endif

#ifdef WITH_BERKELEYDB
#include <db_cxx.h>
#endif


namespace dbtune {

  template <typename Key, typename T>
  class mem_hash_table {
  public:
      using key_type = Key;
      using mapped_type = T;

      using storage_type = phmap::parallel_flat_hash_map<key_type, mapped_type>;
      using iterator = typename storage_type::iterator;

      ~mem_hash_table() {
          if (create_) {
              phmap::BinaryOutputArchive out((name_ + NAME).c_str());
              store_.phmap_dump(out);
          }
      } // ~mem_hash_table


      bool create(const std::string& name) {
          if (!std::filesystem::exists(name)) {
              if (!std::filesystem::create_directory(name)) return false;
          }

          name_ = name;
          create_ = true;

          return true;
      }; // create

      bool load(const std::string& name) {
          phmap::BinaryInputArchive in((name + NAME).c_str());
          return store_.phmap_load(in);
      }; // load


      auto size() const { return store_.size(); }


      std::tuple<bool, iterator, mapped_type> find(const key_type& key) {
          iterator iter = store_.find(key);
          if (iter != store_.end()) return {true, iter, iter->second};
          return {false, iter, mapped_type()};
      } // find

      void insert(const key_type& key, const mapped_type& value) {
          store_.insert({key, value});
      } // insert

      void update(iterator iter, const mapped_type& value) {
          iter->second = value;
      } // update


  private:
      storage_type store_;
      std::string name_;
      bool create_ = false;

      const std::string NAME = "/mht.bin";

  }; // class mem_hash_table


  #ifdef WITH_ROCKSDB

  template <typename Key, typename T>
  class rocks_db {
  public:
      static_assert(std::is_trivially_copyable<Key>::value, "RocksDB key_type must by trivially copyable");
      static_assert(std::is_trivially_copyable<T>::value, "RocksDB mapped_type must by trivially copyable");

      using key_type = Key;
      using mapped_type = T;

      // since we do not have real iterator we use iterator
      // to store key for update operation
      using iterator = std::string;


      ~rocks_db() {
          if (db_ != nullptr) {
              db_->Close();
              delete db_;
          }
      } // ~rockd_db


      auto size() const { return -1; }


      bool create(const std::string& name) { return m_open_db__(name, true); }

      bool load(const std::string& name) { return m_open_db__(name, false); }

      std::tuple<bool, iterator, mapped_type> find(const key_type& key) {
          std::string k(reinterpret_cast<const char*>(&key), sizeof(key));
          std::string value;

          rocksdb::Status s = db_->Get(r_options_, k, &value);

          if (s.ok()) {
              mapped_type v;
              std::memcpy(&v, value.c_str(), sizeof(v));
              return {true, k, v};
          }

          return {false, iterator(), mapped_type()};
      } // find

      void insert(const key_type& key, const mapped_type& value) {
          std::string k(reinterpret_cast<const char*>(&key), sizeof(key));
          std::string v(reinterpret_cast<const char*>(&value), sizeof(value));
          db_->Put(w_options_, k, v);
      } // insert

      void update(const iterator& iter, const mapped_type& value) {
          std::string v(reinterpret_cast<const char*>(&value), sizeof(value));
          db_->Put(w_options_, iter, v);
      } // update


  private:
      bool m_open_db__(const std::string& name, bool write_mode) {
          options_.create_if_missing = write_mode;

          options_.keep_log_file_num = 1;
          options_.recycle_log_file_num = 1;
          options_.paranoid_checks = false;

          options_.allow_mmap_reads = true;
          options_.allow_mmap_writes = true;
          options_.unordered_write = true;
          options_.write_buffer_size = 512 * 1024 * 1024;

          w_options_.disableWAL = true;
          r_options_.verify_checksums = false;

          rocksdb::Status s;

          if (write_mode == true) {
              s = rocksdb::DB::Open(options_, name, &db_);
              if (!s.ok()) return false;
          } else {
              options_.OptimizeForPointLookup(1024);
              s = rocksdb::DB::OpenForReadOnly(options_, name, &db_);
              if (!s.ok()) return false;
          }

          return true;
      } // m_open_db__

      rocksdb::WriteOptions w_options_;
      rocksdb::ReadOptions r_options_;
      rocksdb::Options options_;

      rocksdb::DB* db_ = nullptr;


  }; // class rocks_db

  #endif // WITH_ROCKSDB


  #ifdef WITH_BERKELEYDB

  template <typename Key, typename T>
  class berkeley_db {
  public:
      static_assert(std::is_trivially_copyable<Key>::value, "BerkeleyDB key_type must by trivially copyable");
      static_assert(std::is_trivially_copyable<T>::value, "BerkeleyDB mapped_type must by trivially copyable");

      using key_type = Key;
      using mapped_type = T;

      // since we do not have real iterator we use iterator
      // to store key for update operation
      using iterator = key_type;


      berkeley_db() : db_(nullptr, 0) { }

      ~berkeley_db() { db_.close(0); }


      auto size() const { return -1; }


      bool create(const std::string& name) { return m_open_db__(name, true); }

      bool load(const std::string& name) { return m_open_db__(name, false); }


      std::tuple<bool, iterator, mapped_type> find(const key_type& key) {
          Dbt k;

          k.set_data(const_cast<key_type*>(&key));
          k.set_ulen(sizeof(key));
          k.set_size(sizeof(key));
          k.set_flags(DB_DBT_USERMEM);

          mapped_type value;
          Dbt v;

          v.set_data(&value);
          v.set_ulen(sizeof(value));
          v.set_size(sizeof(value));
          v.set_flags(DB_DBT_USERMEM);

          auto res = db_.get(nullptr, &k, &v, 0);

          if (res == DB_NOTFOUND) return {false, iterator(), mapped_type()};
          return {true, key, value};
      } // find

      void insert(const key_type& key, const mapped_type& value) {
          Dbt k;

          k.set_data(const_cast<key_type*>(&key));
          k.set_ulen(sizeof(key));
          k.set_size(sizeof(key));
          k.set_flags(DB_DBT_USERMEM);

          Dbt v;

          v.set_data(const_cast<mapped_type*>(&value));
          v.set_ulen(sizeof(value));
          v.set_size(sizeof(value));
          v.set_flags(DB_DBT_USERMEM);

          db_.put(nullptr, &k, &v, 0);
      } // insert

      void update(const iterator& iter, const mapped_type& value) {
          insert(iter, value);
      } // update


  private:
      bool m_open_db__(const std::string& name, bool create) {
          db_.set_cachesize(1, 0, 0);

          if (create) {
              if (!std::filesystem::exists(name)) {
                  if (!std::filesystem::create_directory(name)) return false;
              }

              if (db_.open(nullptr, (name + NAME).c_str(), nullptr, DB_HASH, DB_CREATE | DB_TRUNCATE, 0)) return false;
          } else {
              if (db_.open(nullptr, (name + NAME).c_str(), nullptr, DB_HASH, DB_RDONLY | DB_THREAD, 0)) return false;
          }

          return true;
      } // m_open_db__


      Db db_;

      const std::string NAME = "/db.bdb";

  }; // class berkeley_db

  #endif // WITH_BERKELEYDB

} // dbtune

#endif // DBTUNE_HPP
