#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <bio/ncbi.hpp>
#include <cxxopts/cxxopts.hpp>

#include <smarten/Logger.hpp>

#include "KrakItDB.hpp"
#include "config.hpp"


int main(int argc, char* argv[]) {
  smrt::Logger::init();
    auto log = spdlog::stdout_color_mt("KrakitDB");

    std::string tree_name;
    std::string taxid_name;
    std::string names_name;
    std::string in_name;
    std::string out_name;

    bool add_all = true;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("tree", "taxonomic tree in NCBI format", cxxopts::value<std::string>(tree_name))
            ("taxid", "accession to taxid mapping in NCBI format", cxxopts::value<std::string>(taxid_name))
            ("names", "taxonomic names in NCBI format", cxxopts::value<std::string>(names_name)->default_value(""))
            ("add-all", "include all reference sequences", cxxopts::value<bool>(add_all)->default_value("false"))
            ("i,in", "directory with FASTA reference sequences", cxxopts::value<std::string>(in_name))
            ("o,out", "directory to store database", cxxopts::value<std::string>(out_name)->default_value("./"))
            ("h,help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("tree") && opt_res.count("taxid") && opt_res.count("in"))) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (!opt_res.unmatched().empty()) {
            std::cout << options.help() << std::endl;
            return 0;
        }
    } catch (const cxxopts::exceptions::exception& e) {
        log->error("{}", e.what());
        return -1;
    }


    // 1. get taxa tree
    log->info("processing taxonomic tree...");

    bio::NCBITaxa taxa_tree;

    if (!taxa_tree.read(tree_name)) {
        log->error("unable to read taxonomic tree");
        return -1;
    }

    if (!taxa_tree.store(KRAKIT_TREE(out_name))) {
        log->error("unable to store taxonomic tree");
        return -1;
    }

    if (!names_name.empty()) {
        log->info("attaching taxonomic names...");

        if (!taxa_tree.read_names(names_name)) {
            log->error("unable to read taxonomic names");
            return -1;
        }

        if (!taxa_tree.store_names(KRAKIT_NAMES(out_name))) {
            log->error("unable to store taxonomic names");
            return -1;
        }
    }

    log->info("taxonomic tree size: {} nodes", taxa_tree.size());


    // 2. get accession to id mapping
    //    we should accelerate this
    log->info("processing accession to taxid mapping (be patient)...");

    bio::NCBIAccession acc2taxid;

    if (!acc2taxid.read(taxid_name)) {
        log->error("unable to read taxonomic mapping");
        return -1;
    }

    log->info("accession to taxid dictionary size: {}", acc2taxid.size());


    // 3. let's iterate over input to build DB
    //    this is only prototype (we need smarted DB mechanism)
    log->info("creating database of reference reads, be patient...");

    KrakItDBHelper DB(log);
    auto [db_size, total, skip, length] = DB.create(in_name, KRAKIT_READS(out_name), taxa_tree, acc2taxid, add_all);

    log->info("processed {} sequences: {} stored, {} uninformative", total, (total - skip), skip);
    log->info("total length: {}", length);
    log->info("database size: {}", (db_size < 0) ? "unable to report" : std::to_string(db_size));

    log->info("finishing...");

    return 0;
} // main
