#ifndef SMARTEN_RUNTIME_HPP
#define SMARTEN_RUNTIME_HPP

// Class: SMARTEnRuntime
class SMARTEnRuntime {
public:

    // Function: log
    // Exposes logger (spdlog) that a user can utilize for reporting to a console.
    auto log();

    // Function: match_search
    // Creates match search query task described by the tuple (*rid*, *qid*, *query*).
    //
    // Parameters:
    // rid - Id of the read. *read_id_type* must be the same as <ReadPreprocess::read_id_type>.
    // qid - Id of the query. *query_id_type* must be the same as <MatchSearch::query_id_type>.
    // query - Query object. *query_type* must be the same as <MatchSearch::query_type>.
    void match_search(const read_id_type& rid, const query_id_type& qid, const query_type& query);

    // Function: match_search
    void match_search(const read_id_type& rid, const query_id_type& qid, query_type&& query);

    // Function: match_extend
    // Creates match search query task described by the tuple (*rid*, *qid*, *query*).
    //
    // Parameters:
    // rid - Id of the read. *read_id_type* must be the same as <ReadPreprocess::read_id_type>.
    // qid - Id of the query. *query_id_type* must be the same as <MatchExtend::query_id_type>.
    // query - Query object. *query_type* must be the same as <MatchExtend::query_type>.
    void match_extend(const read_id_type& rid, const query_id_type& qid, const query_type& query);

    // Function: match_extend
    void match_extend(const read_id_type& rid, const query_id_type& qid, query_type&& query) {

    // Function: label_assign
    // Creates label assignment task for read *rid*.
    //
    // Parameters:
    // rid    - Id of the read to assign label to.
    //          *read_id_type* must be the same as <ReadPreprocess::read_id_type>.
    // result - Partial result that will be aggregated in the assignment process.
    //          *query_result_type* must be the same as <MatchExtend::query_result_type>.
    void label_assign(const read_id_type& rid, const query_result_type& result);

    // Function: output
    // Creates output task to store result of the label assignment for read *rid*.
    //
    // Parameters:
    // rid    - Id of the read to assign label to.
    //          *read_id_type* must be the same as <ReadPreprocess::read_id_type>.
    // result - Final result (label) assigned to the read.
    //          *result_type* must be the same as <LabelAssign::result_type>.
    void output(const read_id_type& rid, const result_type& result);

}; // class SMARTEnDummyRT

#endif // SMARTEN_RUNTIME_HPP
