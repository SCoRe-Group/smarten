#ifndef MATCH_EXTEND_HPP
#define MATCH_EXTEND_HPP

// Class: MatchExtend
//        This class abstracts match extending.
class MatchExtend {
public:
    // Type: query_id_type
    // Type used to represent match extend query identifier.
    // Note that this can be different than <MatchSearch::query_id_type>.
    using query_id_type = user_define_type;

    // Type: query_type
    // Type used to represent match extend query.
    // Note that this can be different than <MatchSearch::query_type>.
    using query_type = user_defined_type;

    // Type: query_result_type
    // Type used to represent result of match query.
    // Note that this can be different than <MatchSearch::query_result_type>.
    using query_result_type = user_defined_type;

    // Function: run
    // Implements match extension process for query represented by *query*.
    // This method is called automatically by the underlying runtime for each
    // query generated via the call to <SMARTEnRuntime::match_extend()>.
    // The method is allowed to call <SMARTEnRuntime::match_extend()> and
    // <SMARTEnRuntime::label_assign()>.
    //
    // Parameters:
    // rid   - Id of the read from which query has been derived.
    //         *ReadIdType* must be the same as <ReadPreprocess::read_id_type>.
    // qid   - Id of the query being processed.
    // query - Query being processed.
    // rt    - Object of type <SMARTEnRuntime> representing the underlying
    //         runtime system.
    template <typename SMARTEnRuntime>
    static void run(const ReadIdType& rid, const query_id_type& qid, const query_type& query,
                    SMARTEnRuntime& rt);

}; // class MatchExtend

#endif // MATCH_EXTEND_HPP
