#ifndef SMARTEN_APP_HPP
#define SMARTEN_APP_HPP

// Class: SMARTEnApp
template <typename ReadPreprocess,
          typename MatchSearch,
          typename MatchExtend,
          typename LabelAssign>
class SMARTEnApp {
public:
    // Type: read_id_type
    // Alias to <ReadPreprocess::read_id_type>.
    using read_id_type = typename ReadPreprocess::read_id_type;

    // Type: mquery_id_type
    // Alias to <MatchSearch::query_id_type>.
    using mquery_id_type = typename MatchSearch::query_id_type;

    // Type: mquery_type
    // Alias to <MatchSearch::query_type>.
    using mquery_type = typename MatchSearch::query_type;

    // Type: mquery_result_type
    // Alias to <MatchSearch::query_result_type>.
    using mquery_result_type = typename MatchSearch::query_result_type;

    // Type: equery_id_type
    // Alias to <MatchExtend::query_id_type>.
    using equery_id_type = typename MatchExtend::query_id_type;

    // Type: equery_type
    // Alias to <MatchExtend::query_type>.
    using equery_type = typename MatchExtend::query_type;

    // Type: equery_result_type
    // Alias to <MatchExtend::query_result_type>.
    using equery_result_type = typename MatchExtend::query_result_type;

    // Type: result_type
    // Alias to <LabelAssign::result_type>.
    using result_type = typename LabelAssign::result_type;


    // Function: log
    // Exposes logger (spdlog) that a user can utilize for reporting to a console.
    auto log();


    // Function: execute
    // Executes the application for given input range.
    //
    // [first, last) - Range containing input data.
    //                 The iterator type *InputIterator* must be at least
    //                 <InputIterator: https://en.cppreference.com/w/cpp/named_req/InputIterator>
    //                 and its underlying value type must be a tuple where
    //                 the first element is of type <read_id_type>
    //                 and the second element is of type <ReadPreprocess::read_type>.
    // out           - Output stream to store final assignment result. *OutputIterator*
    //                 must be of type <OutputIterator: https://en.cppreference.com/w/cpp/named_req/OutputIterator>
    //                 that is able to store objects of type <result_type>.
    template <typename InputIterator, typename OutputIterator>
    bool execute(InputIterator& first, InputIterator& last, OutputIterator& out);

}; // class SMARTEnApp

#endif // SMARTEN_APP_HPP
