#ifndef READ_PREPROCESS_HPP
#define READ_PREPROCESS_HPP

// Class: ReadPreprocess
//        This class abstracts initial read pre-processing.
class ReadPreprocess {
public:
    // Type: read_id_type
    // Type used to represent identifier of a read.
    using read_id_type = user_defined_type;

    // Type: read_type
    // Type used to represent a read.
    using read_type = user_defined_type;

    // Function: run
    // Implements the actual read pre-processing steps.
    // This method is called automatically by the underlying runtime
    // for each input read. The method is allowed to call any method
    // exposed by the runtime. For example, in the most common scenarios
    // it will call <SMARTEnRuntime::match_search()> or <SMARTEnRuntime::match_extend()>
    // for some selected sub-sequences of the read.
    //
    // Parameters:
    // rid  - Id of the read being processed.
    // read - Read being processed.
    // rt   - Object of type <SMARTEnRuntime> representing the underlying
    //        runtime system.
    template <typename SMARTEnRuntime>
    static void run(read_id_type&& rid, read_type&& read, SMARTEnRuntime& rt);

}; // class ReadPreprocess

#endif // READ_PREPROCESS_HPP
