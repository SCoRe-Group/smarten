#ifndef MATCH_SEARCH_HPP
#define MATCH_SEARCH_HPP

// Class: MatchSearch
//        This class abstracts match searching.
class MatchSearch {
public:

    // Type: query_id_type
    // Type used to represent match query identifier.
    using query_id_type = user_define_type;

    // Type: query_type
    // Type used to represent match query.
    using query_type = user_defined_type;

    // Type: query_result_type
    // Type used to represent result of match query.
    using query_result_type = user_defined_type;

    // Function: run
    // Implements matching process for query represented by *query*
    // against the reference database managed by the underlying runtime.
    // This method is called automatically by the runtime for each query generated
    // via the call to <SMARTEnRuntime::match_search()>. The method is allowed
    // to call any method exposed by the runtime, including creation of new queries
    // via <SMARTEnRuntime::match_search()> or <SMARTEnRuntime::match_extend()>.
    //
    // Parameters:
    // rid   - Id of the read from which query has been derived.
    //         *ReadIdType* must be the same as <ReadPreprocess::read_id_type>.
    // query - Query being processed.
    // rt    - Object of type <SMARTEnRuntime> representing the underlying
    //         runtime system.
    template <typename SMARTEnRuntime>
    static void run(const ReadIdType& rid, const query_id_type& qid, const query_type& query,
                    SMARTEnRuntime& rt);

}; // class MatchSearch

#endif // MATCH_SEARCH_HPP
