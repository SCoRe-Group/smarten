#ifndef LABEL_ASSIGN_HPP
#define LABEL_ASSIGN_HPP

// Class: LabelAssign
//        This class abstracts label assignment process.
class LabelAssign {
public:
    // Type: result_type
    // Type used to represent final output of the classifier.
    using result_type = user_defined_type;

    // Function: run
    // Implements the actual process of assigning taxonomic label
    // to the read identified by *rid*. This method is called automatically
    // by the underlying runtime, and it aggregates all query results that
    // are relevant to the read *rid*. This means all results that have been
    // passed to the runtime via the call to <SMARTEnRuntime::label_assign()>.
    // It is expected that this method will call <SMARTEnRuntime::output()>
    // to finalize classification of the read *rid*.
    //
    // Parameters:
    // rid           - Id of the read to classify. *ReadIdType* must be the same
    //                 as <ReadPreprocess::read_id_type>.
    // [first, last) - Range with results of the queries executed for the read.
    //                 The iterator type *QueryResultIterator* is guaranteed to
    //                 be at least <ForwardIterator: https://en.cppreference.com/w/cpp/named_req/ForwardIterator>.
    //                 The range stores objects of type <MatchExtend::query_result_type>.
    // rt            - Object of type <SMARTEnRuntime> representing the underlying
    //                 runtime system.
    template <typename QueryResultIterator, typename SMARTEnRuntime>
    static void run(const ReadIdType& rid, QueryResultIterator first, QueryResultIterator last,
                    SMARTEnRuntime& rt);

}; // class LabelAssign

#endif // LABEL_ASSIGN_HPP
