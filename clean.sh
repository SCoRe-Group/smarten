#!/bin/bash

VER=`cat VERSION`
SUB=`git rev-parse --short HEAD`

echo "
#ifndef SMRT_CONFIG_HPP
#define SMRT_CONFIG_HPP
#define SMARTEN_VERSION \"$VER.$SUB\"
#endif // SMRT_CONFIG_HPP
" > include/smarten/config.hpp

./nd.sh clean

cd example
make clean
cd ..

cd krakit
./clean.sh
cd ..

cd coriolis
./clean.sh
cd ..

cd tools
make clean
