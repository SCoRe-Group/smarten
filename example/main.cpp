// This is a small backbone to get you started with SMARTEn
// This example implements a trivial app that classifies
// sequences as GC-rich or not

#include <smarten/fastx_iterator.hpp>     // handful of iterators to work with FASTA and FASTQ
#include <smarten/SMARTEnBasicApp.hpp>    // in production you probably want SMARTEnTBBApp or SMARTEnThreadApp
//#include <smarten/SMARTEnThreadApp.hpp>

#include <iostream>
#include <iterator>
#include <vector>


// See: jzola.org/smarten/api/#CClass:ReadPreprocess
class ReadPreprocess {
public:
    // these definitions are mandatory
    using read_id_type = std::string;
    using read_type = std::string;

    // NOTE: if you need your pre-processing routine to store some data
    // you can use static attributes/methods
    // here we keep the sequence length threshold
    static inline int LT = 32;

    template <typename SMARTEnRuntime>
    static void run(read_id_type&& rid,
                    read_type&& read,
                    SMARTEnRuntime& rt) {
        // this is the main read pre-processing method
        // act on the read and use rt to pass the data
        // to the subsequent stages

        // here we check whether the read should be classified
        // or not, unclassified reads are labeled with -1
        if (read.length() < LT) {
            rt.output(rid, -1);
        } else {
            // we divide the read into two parts
            // just because we hope to run in parallel :-)
            int l = read.length();
            int p = l >> 1;
            rt.match_search(rid, l, {read.begin(), read.begin() + p});
            rt.match_search(rid, l, {read.begin() + p, read.end()});
        }
    } // run
}; // class ReadPreprocess


// See: jzola.org/smarten/api/#CClass:MatchSearch
class MatchSearch {
public:
    // these definitions are mandatory
    using query_id_type = int;
    using query_type = typename ReadPreprocess::read_type;
    using query_result_type = float;

    // NOTE: if you need your routine to store some data (e.g., reference db)
    // you can use static attributes/methods

    template <typename SMARTEnRuntime>
    static void run(const ReadPreprocess::read_id_type& rid,
                    query_id_type qid,
                    query_type query,
                    SMARTEnRuntime& rt) {
        // this is the main routine that "gathers evidence"
        // for classifying the read identified by rid

        // in our example, we simply count GC content
        // note that we use qid to store the read length
        // and query to store the read fragment
        // alternatively, we could make query_type a tuple
        // that stores both
        int gc = 0;
        for (auto c : query) gc += ((c == 'C') || (c == 'G'));
        std::cout << rid << " " << gc << " " << qid << std::endl;
        rt.label_assign(rid, (1.0 * gc ) / qid);
    } // run

}; // class MatchSearch


// See: jzola.org/smarten/api/#CClass:MatchExtend
class MatchExtend {
public:
    // these definitions are mandatory
    using query_id_type = int;
    using query_type = int;
    using query_result_type = MatchSearch::query_result_type;

    // NOTE: MatchExtend is optional, and you can make it NO-OP
    // however in such case, the query_result_type must be the same
    // as the MatchSearch::query_result_type

    template <typename SMARTEnRuntime>
    static void run(const ReadPreprocess::read_id_type& rid,
                    query_id_type qid,
                    query_type query,
                    SMARTEnRuntime& rt) { }

}; // class MatchExtend


// See: jzola.org/smarten/api/#CClass:LabelAssign
class LabelAssign {
public:
    // these definitions are mandatory
    using result_type = int;

    // NOTE: if you need your routine to store some data (e.g., a taxa tree)
    // you can use static attributes/methods
    // here we store the GC content threshold
    static inline float GCT = 0.65;

    template <typename QueryResultIterator, typename SMARTEnRuntime>
    static void run(const ReadPreprocess::read_id_type& rid,
                    QueryResultIterator first,
                    QueryResultIterator last,
                    SMARTEnRuntime& rt) {
        // this is the main routine that based on the evidence
        // in the range [first, last)
        // classifies the read identified by rid

        // here we sum up the partial GC content
        // and compare to the threshold
        float gc = 0.0;
        for (; first != last; ++first) gc += *first;

        // reads that are GC-rich are labeled with 1
        // others with 0
        if (gc > GCT) rt.output(rid, 1);
        else rt.output(rid, 0);
    } // run

}; // class LabelAssign


int main(int argc, char* argv[]) {
    if (argc < 2) return -1;

    // combine your types into the final application
    smrt::SMARTEnBasicApp<ReadPreprocess, MatchSearch, MatchExtend, LabelAssign> app;
    //smrt::SMARTEnThreadApp<ReadPreprocess, MatchSearch, MatchExtend, LabelAssign> app;

    // create the input iterator
    // you can use any of the SMARTEn provided input iterator types
    // See: jzola.org/smarten/api/#File3:fastx_iterator.hpp
    smrt::fastx_files_iterator<smrt::fasta_input_iterator> iter(argv[1]), end;

    // create output iterator, here we collect the output into a vector
    // typically you will want to output into some file instead
    std::vector<std::pair<ReadPreprocess::read_id_type, LabelAssign::result_type>> vec;
    auto out = std::back_inserter(vec);

    // and let the magic happen!!!
    app.execute(iter, end, out);

    // let's see the output
    for (auto v : vec) {
        std::cout << v.first << "\t" << v.second << std::endl;
    }

    return 0;
} // main
