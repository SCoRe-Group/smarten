/***
 *  $Id$
 **
 *  File: SMARTEnTBaseApp.hpp
 *  Created: Jun 20, 2024
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright 2024 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SMARTEN_TBASE_APP_HPP
#define SMARTEN_TBASE_APP_HPP

#include "Logger.hpp"
#include "SMARTEnTBaseRT.hpp"
#include "circular_buffer.hpp"
#include "config.hpp"

#include <atomic>
#include <exception>
#include <memory>
#include <mutex>
#include <vector>

#include <jaz/timer.hpp>
#include <parallel_hashmap/phmap.h>


namespace smrt {

  template <typename ReadPreprocess,
            typename MatchSearch,
            typename MatchExtend,
            typename LabelAssign,
            typename TaskManager>
  class SMARTEnTBaseApp {
  public:
      using read_preprocess = ReadPreprocess;
      using match_search = MatchSearch;
      using match_extend = MatchExtend;
      using label_assign = LabelAssign;

      using read_id_type = typename ReadPreprocess::read_id_type;
      using mquery_id_type = typename MatchSearch::query_id_type;
      using mquery_type = typename MatchSearch::query_type;
      using mquery_result_type = typename MatchSearch::query_result_type;
      using equery_id_type = typename MatchExtend::query_id_type;
      using equery_type = typename MatchExtend::query_type;
      using equery_result_type = typename MatchExtend::query_result_type;
      using result_type = typename LabelAssign::result_type;


      explicit SMARTEnTBaseApp(int p = std::thread::hardware_concurrency()) : p_(p) {
          Logger::init();
      } // SMARTEnTBaseApp

      SMARTEnTBaseApp(const SMARTEnTBaseApp&) = delete;
      SMARTEnTBaseApp& operator=(const SMARTEnTBaseApp&) = delete;


      auto log() { return log_; }

      void setp(int p) { p_ = p; }

      template <typename InputIterator, typename OutputIterator>
      bool execute(InputIterator& first, InputIterator& last, OutputIterator& out) {
          log()->info("processing, be patient...");

          jaz::timer T;
          auto te = T.elapsed();

          int p = p_;

          if (p < 1) {
              p = 1;
              log()->warn("too few threads requested, correcting to p={}", p);
          }

          TaskManager tm(p);

          log()->debug("using {} threads...", p);

          double rps = 0.0;

          int nreads = 0;
          int ntasks = 0;

          std::pair<int, int> pos;

          for (; first != last; ++first, ++nreads) {
              pos = input_.put(std::move(*first));

              if (pos.first > -1) {
                  tm.run([self = this, p = pos.first, out]{ self->m_process_task__(p, out); });
                  ntasks++;
              }

              auto tc = T.elapsed();

              // get out update no earlier than 10s
              if ((tc - te) > 10) {
                  te = tc;
                  int pr = preads_.load(std::memory_order_relaxed);
                  rps = (1.0 * pr) / tc;
                  log()->info("processed {} reads at {:.2f} reads/s...", pr, rps);
              }
          } // for first

          // there could be leftovers in the buffer
          pos = input_.put();

          if ((pos.first != -1) && (pos.second > 0)) {
              tm.run([self = this, p = pos.first, out]{ self->m_process_task__(p, out); });
              ntasks++;
          }

          log()->debug("total of {} tasks created", ntasks);

          // get eta
          int pr = preads_.load(std::memory_order_relaxed);
          double eta = (rps > 0.0) ? 1.0 * (nreads - pr) / rps : 0.0;

          log()->info("almost done, {} reads remaining, ETA: {:.1f}s...", (nreads - pr), eta);

          // wait to finish
          tm.wait();

          if (input_.size() > 0) {
              log()->warn("unexpected {} tasks remain in the input buffer!", input_.size());
          }

          // provide info to happy user...
          log_throughput(log(), T.elapsed(), nreads);
          log()->info("done!");

          return true;
      } // execute

  protected:
      int p_ = 0;

  private:
      using input_read_type = std::tuple<read_id_type, typename ReadPreprocess::read_type>;
      using match_query_type = std::tuple<read_id_type, mquery_id_type, mquery_type>;
      using extend_query_type = std::tuple<read_id_type, equery_id_type, equery_type>;
      using output_type = std::pair<read_id_type, result_type>;

      using match_query_queue_type = std::vector<match_query_type>;
      using extend_query_queue_type = std::vector<extend_query_type>;
      using label_dictionary_type = phmap::node_hash_map<read_id_type, std::vector<equery_result_type>>;
      using output_queue_type = std::vector<std::pair<read_id_type, result_type>>;

      friend class SMARTEnTBaseRT<SMARTEnTBaseApp,
                                  match_query_queue_type,
                                  extend_query_queue_type,
                                  label_dictionary_type,
                                  output_queue_type>;


      template <typename OutputIterator>
      void m_process_task__(int pos, OutputIterator& out) {
          match_query_queue_type match_queue;
          extend_query_queue_type extend_queue;
          label_dictionary_type label_dict;
          output_queue_type output_queue;

          auto rt = make_rt(*this, match_queue, extend_queue, label_dict, output_queue);
          int nreads = 0;

          try {
              auto reads = input_.get(pos);
              if (reads.empty()) return;

              nreads = reads.size();

              for (auto& r : reads) {
                  ReadPreprocess::run(std::move(std::get<0>(r)), std::move(std::get<1>(r)), rt);

                  for (auto& q : match_queue) {
                      MatchSearch::run(std::get<0>(q), std::get<1>(q), std::get<2>(q), rt);
                  }

                  match_queue.clear();


                  for (auto& q : extend_queue) {
                      MatchExtend::run(std::get<0>(q), std::get<1>(q), std::get<2>(q), rt);
                  }

                  extend_queue.clear();


                  for (auto& q : label_dict) {
                      LabelAssign::run(q.first, std::begin(q.second), std::end(q.second), rt);
                  }

                  label_dict.clear();
              } // for r
          } catch(std::exception& e) {
              log()->error("whoops, exception from user code? {}", e.what());
              throw;
          } catch (...) {
              log()->error("whoops, unknown exception from user code?");
              throw;
          }

          out_mtx_.lock();
          std::move(std::begin(output_queue), std::end(output_queue), out);
          out_mtx_.unlock();

          preads_.fetch_add(nreads, std::memory_order_relaxed);
      } // m_process_task__


      circular_block_buffer<input_read_type, 128> input_{4};
      std::mutex out_mtx_;

      std::atomic_int preads_ = 0;

      std::shared_ptr<spdlog::logger> log_ = spdlog::stdout_color_mt("SMARTEnApp");

  }; // class SMARTEnTBaseApp

} // namespace smrt

#endif // SMARTEN_TBASE_APP_HPP
