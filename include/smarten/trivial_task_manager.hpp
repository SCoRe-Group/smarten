/***
 *  $Id$
 **
 *  File: trivial_task_manager.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright 2024 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef TRIVIAL_TASK_MANAGER_HPP
#define TRIVIAL_TASK_MANAGER_HPP

#include <atomic>
#include <thread>
#include <vector>

#include <rigtorp/SPSCQueue.h>


namespace smrt {

  class trivial_task_manager {
  public:
      explicit trivial_task_manager(int p, int b = 64)
          : p_{p}, queue_(p), thr_(p) {

          // create thread queues
          for (auto& q : queue_) q = queue_pointer(new queue_type(b));

          // clear finish flags
          wait_.clear();
          done_.clear();

          for (int i = 0; i < p_; ++i) {
              thr_[i] = std::jthread(&trivial_task_manager::m_run__, this, i);
          }
      } // trivial_task_manager

      ~trivial_task_manager() {
          finalize();
      } // ~trivial_task_manager

      template <typename F>
      void run(F f) {
          while (!queue_[qpos_]->try_push(f)) {
            qpos_ = (qpos_ + 1) % p_;
          }
          qpos_ = (qpos_ + 1) % p_;
      } // run

      void wait() {
          wait_.test_and_set();

          while (count_.load() < p_);
          count_.store(0);

          wait_.clear();
          wait_.notify_all();
      } // wait

      void finalize() {
          done_.test_and_set();
          while (count_.load(std::memory_order_acquire) < p_);
      } // finalize


  private:
      using queue_type = rigtorp::SPSCQueue<std::function<void()>>;
      using queue_pointer = std::unique_ptr<queue_type>;

      std::vector<queue_pointer> queue_;
      int qpos_ = 0;

      std::vector<std::jthread> thr_;
      int p_;

      std::atomic_flag wait_ = ATOMIC_FLAG_INIT;
      std::atomic_flag done_ = ATOMIC_FLAG_INIT;

      std::atomic_int count_ = 0;


      void m_run__(int tid) {
          queue_type* Q = queue_[tid].get();

          while (true) {
              auto ptr = Q->front();

              if (ptr != nullptr) {
                  std::function<void()> f = std::move(*ptr);
                  Q->pop();
                  f();
              } else {
                  if (wait_.test()) {
                      // main thread called wait
                      if (Q->empty()) {
                          count_.fetch_add(1);
                          wait_.wait(true);
                      }
                      continue;
                  }
                  if (done_.test()) {
                      // main thread called finanlize
                      if (Q->empty()) break;
                  }
              }
          } // while

          count_.fetch_add(1, std::memory_order_release);
      } // m_run__

  }; // class trivial_task_manager

} // namespace smrt

#endif // TRIVIAL_TASK_MANAGER_HPP
