/***
 *  $Id$
 **
 *  File: SMARTEnBasicRT.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright 2019-2022 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SMRT_SMARTEN_BASIC_RT_HPP
#define SMRT_SMARTEN_BASIC_RT_HPP


namespace smrt {

  template <typename AppType,
            typename MatchQueue,
            typename ExtendQueue,
            typename LabelDict,
            typename OutputQueue> class SMARTEnBasicRT {
  public:
      using read_id_type = typename AppType::read_id_type;
      using mquery_id_type = typename AppType::mquery_id_type;
      using mquery_type = typename AppType::mquery_type;
      using mquery_result_type = typename AppType::mquery_result_type;
      using equery_id_type = typename AppType::equery_id_type;
      using equery_type = typename AppType::equery_type;
      using equery_result_type = typename AppType::equery_result_type;
      using result_type = typename AppType::result_type;

      SMARTEnBasicRT(AppType& app,
                   MatchQueue& match_queue,
                   ExtendQueue& extend_queue,
                   LabelDict& label_dict,
                   OutputQueue& output_queue)
          : app_(app),
            match_queue_(match_queue),
            extend_queue_(extend_queue),
            label_dict_(label_dict),
            output_queue_(output_queue) { }

      auto log() { return app_.log(); }

      void match_search(const read_id_type& rid, const mquery_id_type& qid, const mquery_type& query) {
          match_queue_.push_back({rid, qid, query});
      } // match_search

      void match_extend(const read_id_type& rid, const equery_id_type& qid, const equery_type& query) {
          extend_queue_.push_back({rid, qid, query});
      } // match_extend

      void label_assign(const read_id_type& rid, const equery_result_type& result) {
          label_dict_[rid].push_back(result);
      } // label_assign

      void output(const read_id_type& rid, const result_type& res) {
          output_queue_.push_back({rid, res});
      } // output

  private:
      AppType& app_;
      MatchQueue& match_queue_;
      ExtendQueue& extend_queue_;
      LabelDict& label_dict_;
      OutputQueue& output_queue_;

  }; // class SMARTEnBasicRT

  template <typename AppType,
            typename MatchQueue,
            typename ExtendQueue,
            typename LabelDict,
            typename OutputQueue>
  inline auto make_rt(AppType& app,
                      MatchQueue& match_queue,
                      ExtendQueue& extend_queue,
                      LabelDict& label_dict,
                      OutputQueue& output_queue) {
      return SMARTEnBasicRT(app, match_queue, extend_queue, label_dict, output_queue);
  } // make_rt

} // namespace smrt

#endif // SMRT_SMARTEN_BASIC_RT_HPP
