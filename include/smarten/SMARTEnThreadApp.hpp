/***
 *  $Id$
 **
 *  File: SMARTEnThreadApp.hpp
 *  Created: Jun 20, 2024
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright 2024 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SMARTEN_THREAD_APP_HPP
#define SMARTEN_THREAD_APP_HPP

#include "SMARTEnTBaseApp.hpp"
#include "trivial_task_manager.hpp"


namespace smrt {

  template <typename ReadPreprocess,
            typename MatchSearch,
            typename MatchExtend,
            typename LabelAssign>
  class SMARTEnThreadApp
      : public SMARTEnTBaseApp<ReadPreprocess,
                               MatchSearch,
                               MatchExtend,
                               LabelAssign,
                               trivial_task_manager> {
  public:
      SMARTEnThreadApp()
          : SMARTEnTBaseApp<ReadPreprocess,
                            MatchSearch,
                            MatchExtend,
                            LabelAssign,
                            trivial_task_manager>(4 * std::thread::hardware_concurrency()) {
          this->log()->info("SMARTEnThreadApp ready!");
      } // SMARTEnThreadApp

  }; // SMARTEnThreadApp

} // namespace smrt

#endif // SMARTEN_THREAD_APP_HPP
