/***
 *  $Id$
 **
 *  File: fastx_iterator.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright 2012-2024 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SMRT_FASTX_ITERATOR_HPP
#define SMRT_FASTX_ITERATOR_HPP

#include <filesystem>
#include <fstream>
#include <istream>
#include <string>
#include <tuple>
#include <utility>

#ifdef WITH_INOTIFY
#include <atomic>
#include <mutex>
#include <thread>
#include <queue>
#include <sys/types.h>
#include <sys/inotify.h>
#include <unistd.h>
#endif // WITH_INOTIFY


namespace smrt {

  // Class: fasta_input_iterator
  //        Input iterator extracting reads in FASTA format
  //        from an input stream.
  class fasta_input_iterator {
  public:
      using iterator_category = std::input_iterator_tag;

      // Type: value_type
      // A tuple of strings representing a read. The first string stores the read id,
      // the second string is the actual read.
      using value_type = std::tuple<std::string, std::string>;

      using difference_type = void;
      using pointer = value_type*;
      using reference = value_type&;

      // Constructor: fasta_input_iterator
      // Constructs the end-of-stream iterator.
      fasta_input_iterator() : state_{false}, is_{0} { }

      // Constructor: fasta_input_iterator
      // Constructs the iterator, and performs the first read from *is*
      // to initialize the cached read.
      //
      // Parameters:
      // is - stream from which to extract the reads.
      fasta_input_iterator(std::istream& is, bool shortn = false)
          : is_{&is}, shortn_{shortn} {
          state_ = (is_ && *is_) ? true : false;
          if (state_) {
              while (*is_) {
                  buf_ = "";
                  std::getline(*is_, buf_);
                  if (!buf_.empty() && (buf_[0] == '>')) break;
              }
              m_read__();
          } // if
      } // fasta_input_iterator

      value_type& operator*() { return value_; }

      value_type* operator->() { return &value_; }

      fasta_input_iterator& operator++() {
          m_read__();
          return *this;
      } // operator++

      fasta_input_iterator operator++(int) {
          fasta_input_iterator tmp = *this;
          m_read__();
          return tmp;
      } // operator++

  private:
      void m_read__() {
          state_ = (is_ && *is_) ? true : false;
          if (state_) {
              // trim sequence name
              auto l = buf_.size() - 1;
              if (buf_[l] == '\r') buf_.resize(l);

              s1_ = (buf_.c_str() + 1);
              s2_ = "";

              // get sequence
              do {
                  buf_ = "";
                  std::getline(*is_, buf_);

                  if (!buf_.empty()) {
                      if ((buf_[0] != ';') && (buf_[0] != '>')) {
                          auto l = buf_.size() - 1;
                          if (buf_[l] == '\r') buf_.resize(l);
                          s2_ += buf_;
                      } else if (buf_[0] == '>') break;
                  }
              } while (*is_);

              // trim name to first space/tab
              if (shortn_) {
                  s1_ = s1_.substr(0, s1_.find_first_of(" \t"));
              }

              value_ = {std::move(s1_), std::move(s2_)};
          } // if
      } // m_read__

      bool state_;
      std::istream* is_;

      bool shortn_;

      std::string s1_;
      std::string s2_;
      std::string buf_;

      value_type value_;

      friend bool operator==(const fasta_input_iterator& lhs, const fasta_input_iterator& rhs) {
          return ((lhs.state_ == rhs.state_) && (!lhs.state_ || (lhs.is_ == rhs.is_)));
      } // operator==

      friend bool operator!=(const fasta_input_iterator& lhs, const fasta_input_iterator& rhs) {
          return !(lhs == rhs);
      } // operator!=

  }; // class fasta_input_iterator


  // Class: fastq_input_iterator
  //        Input iterator extracting reads in FASTQ format
  //        from an input stream.
  //
  // Parameters:
  // Score - if true, the reads are extracted together with the quality scores,
  //         otherwise the quality scores are ignored.
  template <bool Score = true> class fastq_input_iterator {
  public:
      using iterator_category = std::input_iterator_tag;

      // Type: value_type
      // A conditional tuple of strings representing a read. The first string stores the read id,
      // the second string is the actual read. If *Score* is true, the third string stores the quality scores.
      using value_type = typename std::conditional<Score, std::tuple<std::string, std::string, std::string>, std::tuple<std::string, std::string>>::type;

      using difference_type = void;
      using pointer = value_type*;
      using reference = value_type&;

      // Constructor: fastq_input_iterator
      // Constructs the end-of-stream iterator.
      fastq_input_iterator() : state_{false}, is_{0} { }

      // Constructor: fastq_input_iterator
      // Constructs the iterator, and performs the first read from *is*
      // to initialize the cached read.
      fastq_input_iterator(std::istream& is, bool shortn = false)
          : is_{&is}, shortn_{shortn} {
          state_ = (is_ && *is_) ? true : false;
          if (state_) {
              while (*is_) {
                  buf_ = "";
                  std::getline(*is_, buf_);
                  if (!buf_.empty() && (buf_[0] == '@')) break;
              }
              m_read__();
          } // if
      } // fastq_input_iterator

      value_type& operator*() { return value_; }

      value_type* operator->() { return &value_; }

      fastq_input_iterator& operator++() {
          m_read__();
          return *this;
      } // operator++

      fastq_input_iterator operator++(int) {
          fastq_input_iterator tmp = *this;
          m_read__();
          return tmp;
      } // operator++

  private:
      void m_read__() {
          state_ = (is_ && *is_) ? true : false;
          if (state_ == true) {
              // trim sequence name
              if (!buf_.empty()) {
                  auto l = buf_.size() - 1;
                  if (buf_[l] == '\r') buf_.resize(l);
              }

              s1_ = (buf_.c_str() + 1);
              s2_ = "";
              s3_ = "";

              // get sequence
              do {
                  buf_ = "";
                  std::getline(*is_, buf_);

                  if (!buf_.empty()) {
                      if (buf_[0] != '+') {
                          auto l = buf_.size() - 1;
                          if (buf_[l] == '\r') buf_.resize(l);
                          s2_ += buf_;
                      } else break;
                  }
              } while (*is_);

              // get scores
              do {
                  buf_ = "";
                  std::getline(*is_, buf_);

                  if (!buf_.empty()) {
                      auto l = buf_.size() - 1;
                      if (buf_[l] == '\r') buf_.resize(l);
                      if (s3_.size() + buf_.size() <= s2_.size()) s3_ += buf_;
                      else {
                          if (buf_[0] != '@') {
                              state_ = false;
                              // we clear stream so that we can
                              // test for eof() outside
                              is_->clear();
                          }
                          break;
                      }
                  } // if
              } while (*is_);

              if (s2_.size() != s3_.size()) {
                  state_ = false;
                  // we clear stream so that we can
                  // test for eof() outside
                  is_->clear();
              } else {
                  // trim name to first space/tab
                  if (shortn_) {
                      s1_ = s1_.substr(0, s1_.find_first_of(" \t"));
                  }

                  if constexpr (Score) {
                      value_ = {std::move(s1_), std::move(s2_), std::move(s3_)};
                  } else {
                      value_ = {std::move(s1_), std::move(s2_)};
                  }
              }

          } // if
      } // m_read__

      bool state_;
      std::istream* is_;

      bool shortn_;

      value_type value_;

      std::string s1_;
      std::string s2_;
      std::string s3_;
      std::string buf_;

      friend bool operator==(const fastq_input_iterator& lhs, const fastq_input_iterator& rhs) {
          return ((lhs.state_ == rhs.state_) && (!lhs.state_ || (lhs.is_ == rhs.is_)));
      } // operator==

      friend bool operator!=(const fastq_input_iterator& lhs, const fastq_input_iterator& rhs) {
          return !(lhs == rhs);
      } // operator!=

  }; // class fastq_input_iterator


  // Class: fastx_files_iterator
  //        Input iterator extracting reads from files in a given directory.
  //        When traversing the input directory, the iterator creates
  //        the actual input iterator of type *Iter* for encountered files.
  //
  // Parameters:
  // Iter - type of the input iterator to extract the reads.
  template <typename Iter = fasta_input_iterator>
  class fastx_files_iterator {
  public:
      using input_iterator_type = Iter;

      using iterator_category = std::input_iterator_tag;

      // Type: value_type
      // Type representing a read. The same as *Iter::value_type*.
      using value_type = typename Iter::value_type;

      using difference_type = void;
      using pointer = value_type*;
      using reference = value_type&;

      // Constructor: fastx_files_iterator
      // Constructs the end-of-stream iterator.
      fastx_files_iterator() : state_{false} { }

      // Constructor: fastx_files_iterator
      // Constructs the iterator, and performs the first read
      // from a file in the directory *dir* to initialize the cached read.
      //
      // Parameters:
      // dir - path to the directory with input reads.
      fastx_files_iterator(const std::filesystem::path& dir, bool shortn = false)
          : state_{true}, end_{}, shortn_{shortn} {
          if (std::filesystem::is_regular_file(dir)) {
              dir_ = end_;
              state_ = true;

              is_.open(dir);
              if (!is_) state_ = false;
              iter_ = input_iterator_type{is_, shortn_};
          } else {
              dir_ = std::filesystem::recursive_directory_iterator{dir, std::filesystem::directory_options::skip_permission_denied};
              m_advance_dir__();

              if (state_ == true) {
                  is_.open(dir_->path());
                  if (!is_) state_ = false;
                  iter_ = input_iterator_type{is_, shortn_};
              }
          }

          m_read__();
      } // fastx_files_iterator


      value_type& operator*() { return value_; }

      value_type* operator->() { return &value_; }


      fastx_files_iterator& operator++() {
          m_read__();
          return *this;
      } // operator++

      fastx_files_iterator operator++(int) {
          fastx_files_iterator tmp = *this;
          m_read__();
          return tmp;
      } // operator++


  private:
      void m_advance_dir__() {
          while ((dir_ != end_) && (std::filesystem::is_regular_file(dir_->path()) == false)) ++dir_;
          state_ = (dir_ != end_);
      } // m_advance_dir__

      void m_read__() {
          if (state_ == true) {
              if (iter_ == input_iterator_type{}) {
                  if (dir_ == end_) {
                      state_ = false;
                      return;
                  }

                  ++dir_;
                  m_advance_dir__();

                  if (state_ == false) return;

                  is_.close();
                  is_.clear();

                  is_.open(dir_->path());

                  if (!is_) {
                      state_ = false;
                      return;
                  }

                  iter_ = input_iterator_type{is_, shortn_};
              } // if iter_

              value_ = std::move(*iter_);
              ++iter_;
          } // if state_
      } // m_read__

      bool state_;

      std::filesystem::recursive_directory_iterator dir_;
      std::filesystem::recursive_directory_iterator end_;

      input_iterator_type iter_;
      std::ifstream is_;

      bool shortn_;

      value_type value_;


      friend bool operator==(const fastx_files_iterator& lhs, const fastx_files_iterator& rhs) {
          return (lhs.state_ == rhs.state_);
      } // operator==

      friend bool operator!=(const fastx_files_iterator& lhs, const fastx_files_iterator& rhs) {
          return !(lhs == rhs);
      } // operator!=

  }; // class fastx_files_iterator


#ifdef WITH_INOTIFY

  // Class: fastx_inotify_iterator
  //        Input iterator extracting reads from files as they are created
  //        in a given directory. The iterator watches the directory
  //        through inotify interface, and creates the actual input iterator
  //        of type *Iter* for files that have signaled IN_CLOSE_WRITE.
  //        To use this iterator the code must be compiled with -DWITH_INOTIFY.
  //
  // Parameters:
  // Iter - type of the input iterator to extract the reads.
  template <typename Iter = fasta_input_iterator>
  class fastx_inotify_iterator {
  public:
      using input_iterator_type = Iter;

      using iterator_category = std::input_iterator_tag;

      // Type: value_type
      // Type representing a read. The same as *Iter::value_type*.
      using value_type = typename Iter::value_type;

      using difference_type = void;
      using pointer = value_type*;
      using reference = value_type&;

      // Constructor: fastx_inotify_iterator
      // Constructs the end-of-stream iterator.
      fastx_inotify_iterator() : state_{false} { }

      // Constructor: fastx_inotify_iterator
      // Constructs the iterator, and performs the first read
      // from a file as soon as it is created in the directory *dir*
      // to initialize the cached read. If no file is created within *tout* seconds,
      // the end-of-stream is reached.
      //
      // Parameters:
      // dir - path to the directory where files are created.
      // tout - timeout before the end-of-stream is reached.
      fastx_inotify_iterator(const std::filesystem::path& dir, int tout = 60, bool shortn = false)
          : dir_{dir}, tout_{tout}, shortn_{shortn} {
          fd_ = inotify_init();

          if (fd_ < 0) {
              state_ = false;
          } else {
              wd_ = inotify_add_watch(fd_, dir.c_str(), IN_CLOSE_WRITE);
              if (wd_ == -1) {
                  state_ = false;
                  close(fd_);
              } else {
                  ev_th_ = std::thread{&fastx_inotify_iterator::m_event_monitor__, this};
                  ev_th_.detach();
                  active_ = true;
                  state_ = true;
              }
          }

          tp_ = std::chrono::steady_clock::now();
      } // fastx_inotify_iterator

      ~fastx_inotify_iterator() {
          if (active_) {
              ev_th_.~thread();
              inotify_rm_watch(fd_, wd_);
              close(fd_);
          }
      } // ~fastx_inotify_iterator


      value_type& operator*() {
          if (!filled_) {
              filled_ = true;
              m_read__();
          }
          return value_;
      } // operator*

      value_type* operator->() {
          if (!filled_) {
              filled_ = true;
              m_read__();
          }
          return &value_;
      } // operator->


      fastx_inotify_iterator& operator++() {
          m_read__();
          return *this;
      } // operator++


  private:
      void m_event_monitor__() {
          const int BUF_SIZE = 65536;
          char buf[BUF_SIZE];

          while (true) {
              int len = read(fd_, buf, BUF_SIZE);

              if (len < 0) {
                  state_ = false;
                  return;
              }

              int i = 0;

              while (i < len) {
                  inotify_event* ev = reinterpret_cast<inotify_event*>(&buf[i]);

                  if (ev->len > 0) {
                      if (ev->mask & IN_CLOSE_WRITE) {
                          q_mtx_.lock();
                          Q_.push(ev->name);
                          q_mtx_.unlock();
                      }

                      i += sizeof(inotify_event) + ev->len;
                  }
              }
          } // while true
      } // m_event_monitor__

      void m_advance_queue__() {
          while (true) {
              q_mtx_.lock();

              if (!Q_.empty()) {
                  fname_ = Q_.front();
                  Q_.pop();
                  q_mtx_.unlock();
                  tp_ = std::chrono::steady_clock::now();
                  break;
              } else {
                  q_mtx_.unlock();

                  auto tc = std::chrono::steady_clock::now();
                  std::chrono::duration<double> diff = tc - tp_;

                  if (diff.count() > tout_) {
                      state_ = false;
                      break;
                  }

                  // we can afford some delay in monitoring FS
                  using namespace std::chrono_literals;
                  std::this_thread::sleep_for(100ms);
              }
          } // while true
      } // m_advance_dir__

      void m_read__() {
          if (state_ == true) {
              if (iter_ == input_iterator_type{}) {
                  m_advance_queue__();

                  if (state_ == false) return;

                  is_.close();
                  is_.clear();

                  is_.open(dir_ + "/" + fname_);

                  if (!is_) {
                      state_ = false;
                      return;
                  }

                  iter_ = input_iterator_type{is_, shortn_};
              } // if iter_

              value_ = std::move(*iter_);
              ++iter_;
          } // if state_
      } // m_read__


      std::thread ev_th_;

      std::chrono::steady_clock::time_point tp_;
      int tout_;

      std::queue<std::string> Q_;
      std::mutex q_mtx_;

      input_iterator_type iter_;
      std::ifstream is_;

      bool shortn_;

      std::string fname_;
      std::string dir_;

      value_type value_;

      bool active_ = false;
      bool filled_ = false;
      std::atomic_bool state_ = false;

      int fd_ = -1;
      int wd_ = -1;


      friend bool operator==(const fastx_inotify_iterator& lhs, const fastx_inotify_iterator& rhs) {
          return (lhs.state_ == rhs.state_);
      } // operator==

      friend bool operator!=(const fastx_inotify_iterator& lhs, const fastx_inotify_iterator& rhs) {
          return !(lhs == rhs);
      } // operator!=

  }; // class fastx_inotify_iterator

#endif // WITH_INOTIFY

} // namespace smrt

#endif // SMRT_FASTX_ITERATOR_HPP
