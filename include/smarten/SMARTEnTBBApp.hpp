/***
 *  $Id$
 **
 *  File: SMARTEnTBBApp.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright 2019-2023 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SMRT_SMARTEN_TBB_APP_HPP
#define SMRT_SMARTEN_TBB_APP_HPP

#include "SMARTEnTBaseApp.hpp"
#include <tbb/task_group.h>


namespace smrt {

  struct task_group_wrap {
      task_group_wrap(int) { }
      template <typename ...Args> void run(Args&& ...args) {
          tg.run(std::forward<Args>(args)...);
      } // run
      void wait() { tg.wait(); }
      tbb::task_group tg;
  }; // struct task_group_wrap

  template <typename ReadPreprocess,
            typename MatchSearch,
            typename MatchExtend,
            typename LabelAssign>
  class SMARTEnTBBApp
      : public SMARTEnTBaseApp<ReadPreprocess,
                               MatchSearch,
                               MatchExtend,
                               LabelAssign,
                               task_group_wrap> {
  public:
      SMARTEnTBBApp()
          : SMARTEnTBaseApp<ReadPreprocess, MatchSearch, MatchExtend, LabelAssign, task_group_wrap>(-1) {
          this->log()->info("SMARTEnTBBApp ready!");
      } // SMARTEnTBBApp

  }; // class SMARTEnTBBApp

} // namespace smrt

#endif // SMRT_SMARTEN_TBB_APP_HPP
