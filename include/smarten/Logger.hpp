/***
 *  $Id$
 **
 *  File: Logger.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright 2019-2022 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SMRT_LOGGER_HPP
#define SMRT_LOGGER_HPP

#include <cstdlib>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>


namespace smrt {

  struct Logger {
      static void init() {
          spdlog::set_pattern("%Y-%m-%d %H:%M:%S [%^%l%$] %n: %v");
          spdlog::set_level(spdlog::level::info);
          if (const char* env = std::getenv("SMARTEN_LOG_LEVEL")) {
              if (std::strncmp(env, "debug", 5) == 0) spdlog::set_level(spdlog::level::debug);
              else if (std::strncmp(env, "info", 4) == 0) spdlog::set_level(spdlog::level::info);
              else if (std::strncmp(env, "warn", 4) == 0) spdlog::set_level(spdlog::level::warn);
              else if (std::strncmp(env, "error", 5) == 0) spdlog::set_level(spdlog::level::err);
          }
      } // init
  }; // struct Logger

  inline void log_throughput(std::shared_ptr<spdlog::logger> log, double t, int nreads) {
      double rps = (1.0 * nreads) / t;
      double krpm = (60 * rps) / 1000;

      if (nreads == 0) {
          log->warn("no input reads, please check your input...");
      } else {
          log->info("processed {} reads in {:.2f} seconds", nreads, t);
          log->info("throughput {:.2f} reads/s ({:.2f} Kseq/min)", rps, krpm);
      }
  } // log_throughput

} // namespace smrt

#endif // SMRT_LOGGER_HPP
