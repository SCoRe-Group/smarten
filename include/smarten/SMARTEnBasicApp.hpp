/***
 *  $Id$
 **
 *  File: SMARTEnBasicApp.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright 2019-2022 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SMRT_SMARTEN_BASIC_APP_HPP
#define SMRT_SMARTEN_BASIC_APP_HPP

#include "Logger.hpp"
#include "SMARTEnBasicRT.hpp"
#include "config.hpp"

#include <exception>
#include <tuple>

#include <jaz/timer.hpp>
#include <parallel_hashmap/phmap.h>


namespace smrt {

  template <typename ReadPreprocess,
            typename MatchSearch,
            typename MatchExtend,
            typename LabelAssign>
  class SMARTEnBasicApp {
  public:
      using read_preprocess = ReadPreprocess;
      using match_search = MatchSearch;
      using match_extend = MatchExtend;
      using label_assign = LabelAssign;

      using read_id_type = typename ReadPreprocess::read_id_type;
      using mquery_id_type = typename MatchSearch::query_id_type;
      using mquery_type = typename MatchSearch::query_type;
      using mquery_result_type = typename MatchSearch::query_result_type;
      using equery_id_type = typename MatchExtend::query_id_type;
      using equery_type = typename MatchExtend::query_type;
      using equery_result_type = typename MatchExtend::query_result_type;
      using result_type = typename LabelAssign::result_type;


      SMARTEnBasicApp() { Logger::init(); }

      SMARTEnBasicApp(const SMARTEnBasicApp&) = delete;
      SMARTEnBasicApp& operator=(const SMARTEnBasicApp&) = delete;


      auto log() { return log_; }


      template <typename InputIterator, typename OutputIterator>
      bool execute(InputIterator& first, InputIterator& last, OutputIterator& out) {
          log()->info("processing reads...");

          jaz::timer T;

          std::vector<input_read_type> buf;
          int nreads = 0;

          for (; first != last; ++first, ++nreads) {
              buf.emplace_back(std::move(*first));
              if (buf.size() == BUF_SIZE_) m_process_task__(buf, out);
          }

          m_process_task__(buf, out);

          // provide info to happy user...
          double t = T.elapsed();
          log_throughput(log(), t, nreads);
          log()->info("done!");

          return true;
      } // execute


  private:
      const int BUF_SIZE_ = 65536;

      using input_read_type = std::tuple<read_id_type, typename ReadPreprocess::read_type>;
      using match_query_type = std::tuple<read_id_type, mquery_id_type, mquery_type>;
      using extend_query_type = std::tuple<read_id_type, equery_id_type, equery_type>;
      using output_type = std::pair<read_id_type, result_type>;

      using match_query_queue_type = std::vector<match_query_type>;
      using extend_query_queue_type = std::vector<extend_query_type>;
      using label_dictionary_type = phmap::node_hash_map<read_id_type, std::vector<equery_result_type>>;
      using output_queue_type = std::vector<std::pair<read_id_type, result_type>>;

      friend class SMARTEnBasicRT<SMARTEnBasicApp,
                                  match_query_queue_type,
                                  extend_query_queue_type,
                                  label_dictionary_type,
                                  output_queue_type>;

      template <typename OutputIterator>
      void m_process_task__(std::vector<input_read_type>& reads, OutputIterator& out) {
          match_query_queue_type match_queue;
          extend_query_queue_type extend_queue;
          label_dictionary_type label_dict;
          output_queue_type output_queue;

          auto rt = make_rt(*this, match_queue, extend_queue, label_dict, output_queue);

          if (reads.empty()) return;

          try {

              for (auto& r : reads) {
                  ReadPreprocess::run(std::move(std::get<0>(r)), std::move(std::get<1>(r)), rt);
              }

              for (auto& q : match_queue) {
                  MatchSearch::run(std::get<0>(q), std::get<1>(q), std::get<2>(q), rt);
              }
              match_queue.clear();
              match_queue.shrink_to_fit();


              for (auto& q : extend_queue) {
                  MatchExtend::run(std::get<0>(q), std::get<1>(q), std::get<2>(q), rt);
              }
              extend_queue.clear();
              extend_queue.shrink_to_fit();


              for (auto& r : label_dict) {
                  LabelAssign::run(r.first, std::begin(r.second), std::end(r.second), rt);
              }
              label_dict.clear();

          } catch(std::exception& e) {
              log()->error("whoops, exception from user code? {}", e.what());
              throw;
          } catch (...) {
              log()->error("whoops, unknown exception from user code?");
              throw;
          }

          std::move(std::begin(output_queue), std::end(output_queue), out);
          reads.clear();
      } // m_process_task__

      std::shared_ptr<spdlog::logger> log_ = spdlog::stdout_color_mt("SMARTEnBasicApp");

  }; // class SMARTEnBasicApp

} // namespace smrt

#endif // SMRT_SMARTEN_BASIC_APP_HPP
