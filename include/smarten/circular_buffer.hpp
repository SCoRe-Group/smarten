/***
 *  $Id$
 **
 *  File: circular_buffer.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright 2019-2024 SCoRe Group <http://www.score-group.org>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SMRT_CIRCULAR_BUFFER_HPP
#define SMRT_CIRCULAR_BUFFER_HPP

#include <atomic>
#include <chrono>
#include <thread>
#include <vector>


namespace smrt {

  using namespace std::chrono_literals;

  // Class: circular_block_buffer
  // Implementation of a circular buffer in which elements
  // are stored in blocks.
  //
  // Parameters:
  // T - type of objects stored in the buffer
  // N - capacity of the buffer in blocks
  template <typename T, int N>
  class circular_block_buffer {
  public:
      static const int CACHE_LINE = 64;
      using value_type = T;

      // Constructor: circular_block_buffer
      //
      // Parameters:
      // bs - block size.
      explicit circular_block_buffer(int bs = 1): bs_(bs) { }


      // Function: put
      // Adds element to the buffer. Put can be called only by a single thread.
      // However, it can be called concurrently with get.
      //
      // Parameters:
      // t - element to add to the buffer.
      //
      // Returns: index and the size of the block that has been filled and can be processed
      // or tuple (-1, 0) if adding element did not fill up a block.
      std::pair<int, int> put(const T& t) {
          m_get_head__();

          auto res = m_commit_put__(t);

          if (res.first != -1) {
              buffer_[head_].lock.clear(std::memory_order_release);
              block_full_ = true;
          }

          return res;
      } // put

      std::pair<int, int> put(T&& t) {
          m_get_head__();

          auto res = m_commit_put__(t);

          if (res.first != -1) {
              buffer_[head_].lock.clear(std::memory_order_release);
              block_full_ = true;
          }

          return res;
      } // put


      // Function: put
      // Releases the current block even if it is empty or not full.
      std::pair<int, int> put() {
          if (block_full_) return {-1, -1}; // the block was already released
          std::pair<int, int> res = { head_, buffer_[head_].block.size() };
          buffer_[head_].lock.clear(std::memory_order_release);
          block_full_ = true;
          return res;
      } // put


      // Function: get
      // Returns the block stored at position *pos*.
      // The function will lock the buffer and move the block
      // to the requesting thread. This method is thread safe,
      // and multiple threads can issue get at the same time.
      //
      // Parameters:
      // pos - position of the block to get.
      [[nodiscard]] std::vector<value_type> get(int pos) {
          std::vector<value_type> v;

          while (buffer_[pos].lock.test_and_set(std::memory_order_acquire));

          v.swap(buffer_[pos].block);

          buffer_[pos].lock.clear(std::memory_order_release);
          buffer_[pos].lock.notify_one(); // only producer should be waiting

          return v; // this better be RVO
      } // get


      // Function: size
      // Returns: the number of non-empty blocks in the buffer.
      // This function is not thread-safe.
      int size() const {
          int S = 0;
          for (int i = 0; i < N; ++i) S += (!buffer_[i].block.empty());
          return S;
      } // size


  private:
      // Returns: locked position that is guaranteed
      // to point to the block to which a new item can be added.
      void m_acquire_head_lock__() {
          int nt = 0;

          while (true) {
              buffer_[head_].lock.wait(true, std::memory_order_acquire);
              while (buffer_[head_].lock.test_and_set(std::memory_order_acquire));

              if (buffer_[head_].block.size() < bs_) {
                  break;
              }

              buffer_[head_].lock.clear(std::memory_order_release);
              head_ = (head_ + 1) % N;

              std::this_thread::sleep_for(nt * BASE_SLEEP_TIME);
              if (nt < 64) nt++; // we don't want to go too far :-)
          }
      } // m_acquire_head_lock__

      void m_get_head__() {
          if (block_full_ == true) {
              m_acquire_head_lock__();
              block_full_ = false;
          }
      } // m_get_head__

      // Returns: index and the size of the block that has been filled and can be processed
      // or tuple (-1, 0) if adding element did not fill up a block.
      std::pair<int, int> m_commit_put__(const T& t) {
          buffer_[head_].block.push_back(t);
          if (buffer_[head_].block.size() < bs_) return {-1, 0};
          return { head_, buffer_[head_].block.size() };
      } // m_commit_put__

      std::pair<int, int> m_commit_put__(T&& t) {
          buffer_[head_].block.emplace_back(t);
          if (buffer_[head_].block.size() < bs_) return {-1, 0};
          return { head_, buffer_[head_].block.size() };
      } // m_commit_put__

      int bs_ = 1;
      int head_ = 0;

      bool block_full_ = true;

      struct alignas(CACHE_LINE) block_type {
          std::vector<value_type> block;
          std::atomic_flag lock;
      }; // block_type

      alignas(CACHE_LINE) block_type buffer_[N];

      static inline const std::chrono::microseconds BASE_SLEEP_TIME = 1us;

  }; // class circular_block_buffer

} // namespace smrt

#endif // SMRT_CIRCULAR_BUFFER_HPP
