# SMARTEn

SMARTEn, System for Mobile Analysis in Real-Time of ENvironment, aims to provide new ways to analyze metagenomic DNA using fully mobile setups. Visit https://cse.buffalo.edu/~jzola/smarten/ to learn more about the project.

This project has been supported by [NSF](https://www.nsf.gov/) under the award [CNS-1910193](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1910193).

**If you are looking for [Coriolis](https://gitlab.com/SCoRe-Group/smarten/-/tree/master/coriolis), it is [here!](https://gitlab.com/SCoRe-Group/smarten/-/tree/master/coriolis)**

## Requirements

 * A C++20-compliant compiler, e.g., `g++-13`.
 * The [spdlog](https://github.com/gabime/spdlog) library version 1.5 or higher.
 * The Intel TBB library (if using the `SMARTEnTBBApp`).

## Getting Started

* The SMARTEn API specification is available from https://cse.buffalo.edu/~jzola/smarten/api.html. The API allows you to build a fully functional metagenomic classifier by implementing only basic building blocks. The runtime system takes care of efficient parallelization and executing on, e.g., multicore processors or edge.
* In this folder https://gitlab.com/SCoRe-Group/smarten/-/tree/master/template you can find code stubs to build your implementation. Specifically, you care about `ReadPreprocess`, `MatchSearch`, `MatchExtend` and `LabelAssign`. `SMARTEnRuntime` and `SMARTEnApp` are on us :-)
* In `example/` we provide a self-explanatory implementation of a trivial classifier that you can use as a guide and starting point for implementing your own classifier.
* We provide also a fully functional classifiers, following the classic ideas used in Kraken/LMAT and Centrifuge, called `krakit` and [`coriolis`](https://gitlab.com/SCoRe-Group/smarten/-/tree/master/coriolis), respectively.
* If you wish to test the framework, please start with building and running [`coriolis`](https://gitlab.com/SCoRe-Group/smarten/-/tree/master/coriolis), which is our current metagenomic classifier.
* Finally, to get you even more examples to work with, we implemented a very simple classifier that tests GC-content of the input reads. Check  [`example/`](https://gitlab.com/SCoRe-Group/smarten/-/tree/master/example) to learn more.

## License and Copyright

© 2019-2024 [SCoRe Group](http://www.score-group.org/). SMARTEn is distributed under the terms of the Apache License 2.0.

## How to Cite

If you use SMARTEn, `krakit`, or `coriolis`, please cite our paper:

> A.J. Mikalsen and J. Zola. "[Coriolis: Enabling metagenomic classification on lightweight mobile devices](https://academic.oup.com/bioinformatics/article/39/Supplement_1/i66/7210430)", In: _Intelligent Systems for Molecular Biology_ (ISMB), 2023.
